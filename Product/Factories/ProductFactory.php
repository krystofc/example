<?php

namespace App\Model\Data\Product;

use App\Model\Cache\EntityCache;
use App\Model\Data\BaseFactory;
use App\Model\Data\Category;
use App\Model\Data\CategoryFactory;
use App\Model\Data\Tag;
use App\Model\Data\Tag\TagFactory;
use App\Model\Database\Table\ProductAttributeTable;
use App\Model\Database\Table\ProductHasTagTable;
use App\Model\Database\Table\ProductPriceTable;
use App\Model\Database\Table\ProductTable;
use App\Model\Database\Table\ProductTextTable;
use Nette\Database\Table\ActiveRow;
use Nette\Database\Table\IRow;


/**
 * ProductFactory
 *
 * @author Kryštof Česal
 *
 * @property callable attributeBuilder
 * @property callable categoryBuilder
 * @property callable tagsBuilder
 * @property callable stockBuilder
 */
class ProductFactory extends BaseFactory {

	/** @var ProductTable */
	protected $productTable;

	/** @var AttributeFactory */
	protected $attributeFactory;

	/** @var CategoryFactory */
	protected $categoryFactory;

	/** @var EntityCache */
	protected $cache;

	/** @var ActiveProduct[] */
	protected $products = [];

	/** @var ProductAttributeTable */
	protected $productAttributeTable;

	/** @var ProductHasTagTable */
	protected $productHasTagTable;

	/** @var ProductPriceTable */
	protected $productPriceTable;

	/** @var ProductTextTable */
	protected $productTextTable;

	/** @var TagFactory */
	protected $tagFactory;

	/** @var AttributeOptionFactory */
	protected $attributeOptionFactory;

	/** @var StockFactory */
	protected $stockFactory;

	/** @var ProductEventDispatcher */
	protected $eventDispatcher;


	/**
	 * @param ProductTable $productTable
	 * @param AttributeFactory $attributeFactory
	 * @param CategoryFactory $categoryFactory
	 * @param EntityCache $cache
	 * @param ProductAttributeTable $productAttributeTable
	 * @param ProductHasTagTable $productHasTagTable
	 * @param ProductTextTable $productTextTable
	 * @param ProductPriceTable $productPriceTable
	 * @param TagFactory $tagFactory
	 * @param AttributeOptionFactory $attributeOptionFactory
	 * @param StockFactory $stockFactory
	 * @param ProductEventDispatcher $dispatcher
	 */
	function __construct(ProductTable $productTable, AttributeFactory $attributeFactory, CategoryFactory $categoryFactory, EntityCache $cache, ProductAttributeTable $productAttributeTable, ProductHasTagTable $productHasTagTable, ProductTextTable $productTextTable, ProductPriceTable $productPriceTable, TagFactory $tagFactory, AttributeOptionFactory $attributeOptionFactory, StockFactory $stockFactory, ProductEventDispatcher $dispatcher) {
		$this->productTable = $productTable;
		$this->attributeFactory = $attributeFactory;
		$this->categoryFactory = $categoryFactory;
		$this->cache = $cache->getNamespace('products');
		$this->productAttributeTable = $productAttributeTable;
		$this->productHasTagTable = $productHasTagTable;
		$this->productPriceTable = $productPriceTable;
		$this->productTextTable = $productTextTable;
		$this->tagFactory = $tagFactory;
		$this->attributeOptionFactory = $attributeOptionFactory;
		$this->stockFactory = $stockFactory;
		$this->eventDispatcher = $dispatcher;
	}


	/**
	 * @param int|IRow $idOrRow
	 * @return ActiveProduct
	 */
	public function create($idOrRow) {
		$id = (string) $idOrRow;
		if (isset($this->products[$id])) {
			return $this->products[$id];
		}
		if (!$product = $this->cache->get($id)) {
			$product = $this->createFromRow($this->getRow($idOrRow, $this->productTable));
		}
		$product->setBuilders($this->categoryBuilder, $this->attributeBuilder, $this->tagsBuilder, $this->stockBuilder);
		$product->setDependencies($this->productTable, $this->productHasTagTable, $this->productPriceTable, $this->productTextTable, $this->attributeFactory, $this->attributeOptionFactory, $this->eventDispatcher);
		$this->products[$product->getId()] = $product;
		return $product;
	}


	/**
	 * @param ActiveRow $row
	 * @return ActiveProduct
	 */
	protected function createFromRow(ActiveRow $row) {
		$texts = [];
		foreach ($row->related('product_text') as $textRow) {
			$texts[(string) $textRow->language_id] = ['name' => $textRow->name, 'about' => $textRow->about];
		}
		$prices = $row->related('product_price')->fetchPairs('currency_id', 'price');
		return new ActiveProduct($row->id, $texts, $prices, $row->production_time, $row->tailored, $row->deleted, $row->visible, $row->seller_id, $row->commission, $row->after_sold_out, (string) $row->category_id, $row->gender, $row->image);
	}


	/**
	 * @param ActiveProduct $product
	 * @return Category
	 */
	public function categoryBuilder(ActiveProduct $product) {
		return $this->categoryFactory->create($product->getCategoryId());
	}


	/**
	 * @param ActiveProduct $product
	 * @return Stock
	 */
	public function stockBuilder(ActiveProduct $product) {
		return $this->stockFactory->create($product);
	}


	/**
	 * @param ActiveProduct $product
	 * @return Attribute[]
	 */
	public function attributeBuilder(ActiveProduct $product) {
		$attributes = [];
		foreach ($this->productAttributeTable->findBy('product_id', $product->getId()) as $productAttributeRow) {
			$productAttribute = $this->attributeFactory->createProductAttribute($productAttributeRow, $product);
			$attributes[$productAttribute->getIdentificator()] = $productAttribute;
		}
		$tagsByType = [];
		foreach ($product->getTags() as $tag) {
			if(!$tag->getTagType()->isSelectable()) {
				continue;
			}
			$tagTypeId = $tag->getTagType()->getId();
			if (!isset($tagsByType[$tagTypeId])) {
				$tagsByType[$tagTypeId] = ['type' => $tag->getTagType(), 'selected' => []];
			}
			array_push($tagsByType[$tagTypeId]['selected'], $tag->getId());
		}
		foreach ($tagsByType as $typeId => $data) {
			$attribute = $this->attributeFactory->createAttributeFromTag($data['type'], $data['selected'], $product);
			$attributes[$attribute->getIdentificator()] = $attribute;
		}
		return $attributes;
	}


	/**
	 * @param Product $product
	 * @return Tag[]
	 */
	public function tagsBuilder(Product $product) {
		$tags = [];
		if ($product->getTagsIds() !== NULL) {
			foreach ($product->getTagsIds() as $tagId) {
				$tags[$tagId] = $this->tagFactory->createTag($tagId);
			}
		} else {
			$selection = $this->productHasTagTable->findBy('product_id', $product->getId());
			foreach ($selection as $tagConnectionRow) {
				$tags[(int) (string) $tagConnectionRow->tag_id] = $this->tagFactory->createTag($tagConnectionRow->tag);
			}
		}
		return $tags;
	}


	/**
	 * @param array $productData
	 * @return ActiveProduct
	 */
	public function createNewProduct($productData) {
		$productData = array_intersect_key(array_filter($productData), array_flip(['tailored', 'after_sold_out',
																				   'production_time', 'seller_id',
																				   'commission', 'visible',
																				   'category_id', 'gender']));
		$productData += ['tailored' => 0, 'after_sold_out' => 0, 'production_time' => 0, 'visible' => 1, 'gender' => 0];
		$row = $this->productTable->insert($productData);
		$product = new ActiveProduct($row->id, [], [], $productData['production_time'], $productData['tailored'], false, $productData['visible'], $productData['seller_id'], $productData['commission'], $productData['after_sold_out'], $productData['category_id'], $productData['gender']);
		$product->setBuilders($this->categoryBuilder, $this->attributeBuilder, $this->tagsBuilder, $this->stockBuilder);
		$product->setDependencies($this->productTable, $this->productHasTagTable, $this->productPriceTable, $this->productTextTable, $this->attributeFactory, $this->attributeOptionFactory, $this->eventDispatcher);
		$this->products[$product->getId()] = $product;
		return $product;
	}


	/**
	 * @return void
	 */
	function __destruct() {
		foreach ($this->products as $product) {
			if (!$this->cache->has($product->getId())) {
				$this->cache->set($product);
			}
		}
	}

} 