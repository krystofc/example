<?php

namespace App\Model\Data\Product;

use App\Model\Data\BaseFactory;
use App\Model\Database\Table\StockHasProductAttributeOptionTable;
use App\Model\Database\Table\StockHasTagTable;
use App\Model\Database\Table\StockTable;


/**
 * StockFactory
 *
 * @author Kryštof Česal
 *
 * @property callable combinationsBuilder
 */
class StockFactory extends BaseFactory {

	/** @var StockTable */
	protected $stockTable;

	/** @var StockHasProductAttributeOptionTable */
	protected $stockHasProductAttributeOptionTable;

	/** @var StockHasTagTable */
	protected $stockHasTagTable;


	/**
	 * StockFactory constructor.
	 * @param StockTable $stockTable
	 * @param StockHasProductAttributeOptionTable $stockHasProductAttributeOptionTable
	 * @param StockHasTagTable $stockHasTagTable
	 */
	public function __construct(StockTable $stockTable, StockHasProductAttributeOptionTable $stockHasProductAttributeOptionTable, StockHasTagTable $stockHasTagTable) {
		$this->stockTable = $stockTable;
		$this->stockHasProductAttributeOptionTable = $stockHasProductAttributeOptionTable;
		$this->stockHasTagTable = $stockHasTagTable;
	}


	/**
	 * @param ActiveProduct $product
	 * @return Stock
	 */
	public function create(ActiveProduct $product) {
		return new Stock($product, $this->combinationsBuilder);
	}


	/**
	 * @param Stock $stock
	 * @return StockCombination[]
	 */
	public function combinationsBuilder(Stock $stock) {
		$combinations = [0 => []];
		foreach ($stock->getProduct()->getAttributes() as $attribute) {
			$newCombinations = [];
			foreach ($attribute->getOptions() as $option) {
				foreach ($combinations as $combinationId => $combination) {
					$newCombinations[] = $combination + [$attribute->getIdentificator() => $option];
				}
			}
			//if($newCombinations)
			$combinations = $newCombinations;
		}
		$fromDb = $this->stockTable->findBy('product_id', $stock->getProduct()->getId())
			->fetchPairs('identificator', 'pieces');
		$combinationObjects = [];
		foreach ($combinations as $combinationArray) {
			$identificator = StockCombination::createIdentificator($combinationArray);
			$combinationObjects[$identificator] = $combination = new StockCombination($combinationArray, isset($fromDb[$identificator]) ? $fromDb[$identificator] : 0, $stock);
			$combination->setDependencies($this->stockTable, $this->stockHasProductAttributeOptionTable, $this->stockHasTagTable);
		}
		return $combinationObjects;
	}
}