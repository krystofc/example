<?php

namespace App\Model\Data\Product;

use App\Model\Data\BaseFactory;
use App\Model\Data\Tag;
use App\Model\Data\TagType;
use App\Model\Database\Table\ProductAttributeOptionTable;
use App\Model\Database\Table\ProductAttributeTable;
use App\Model\Database\Table\ProductAttributeTextTable;
use App\Model\Database\Table\TagTypeTable;
use Nette\Database\Table\IRow;


/**
 * AttributeFactory
 *
 * @author Kryštof Česal
 *
 * @property callable productAttributeOptionsBuilder
 */
class AttributeFactory extends BaseFactory {

	/** @var ProductAttributeTable */
	protected $productAttributeTable;

	/** @var TagTypeTable */
	protected $tagTypeTable;

	/** @var AttributeOptionFactory */
	protected $attributeOptionFactory;

	/** @var Attribute */
	protected $attributes = [];

	/** @var ProductAttributeOptionTable */
	protected $optionTable;

	/** @var ProductAttributeTextTable */
	protected $productAttributeTextTable;


	/**
	 * @param AttributeOptionFactory $attributeOptionFactory
	 * @param ProductAttributeTable $productAttributeTable
	 * @param TagTypeTable $tagTypeTable
	 * @param ProductAttributeOptionTable $productAttributeOptionTable
	 * @param ProductAttributeTextTable $productAttributeTextTable
	 */
	function __construct(AttributeOptionFactory $attributeOptionFactory, ProductAttributeTable $productAttributeTable, TagTypeTable $tagTypeTable, ProductAttributeOptionTable $productAttributeOptionTable, ProductAttributeTextTable $productAttributeTextTable) {
		$this->attributeOptionFactory = $attributeOptionFactory;
		$this->productAttributeTable = $productAttributeTable;
		$this->tagTypeTable = $tagTypeTable;
		$this->optionTable = $productAttributeOptionTable;
		$this->productAttributeTextTable = $productAttributeTextTable;
	}


	/**
	 * @param int|IRow $idOrRow
	 * @param ActiveProduct $product
	 * @return ProductAttribute
	 */
	public function createProductAttribute($idOrRow, ActiveProduct $product) {
		$id = (string) $idOrRow;
		if (isset($this->attributes[$id])) {
			return $this->attributes[$id];
		}
		$row = $this->getRow($idOrRow, $this->productAttributeTable);
		$names = $row->related('product_attribute_text')->fetchPairs('language_id', 'name');
		$attribute = new ProductAttribute($this->productAttributeOptionsBuilder, $product, $row->id, $names);
		return $this->buildProductAttributeDependencies($attribute);
	}


	/**
	 * @param TagType $tagType
	 * @param array $selected
	 * @param ActiveProduct $product
	 * @return AttributeFromTag
	 */
	public function createAttributeFromTag(TagType $tagType, array $selected, ActiveProduct $product) {
		$identificator = AttributeFromTag::createIdentificator($product, $tagType);
		if (isset($this->attributes[$identificator])) {
			return $this->attributes[$identificator];
		}
		$optionsBuilder = function (AttributeFromTag $attribute) use ($selected) {
			$tags = [];
			foreach ($attribute->getTagType()->getTags() as $tag) {
				if (in_array($tag->getId(), $selected)) {
					$option = $this->attributeOptionFactory->createAttributeOptionFromTag($tag, $attribute);
					$tags[$option->getIdentificator()] = $option;
				}
			}
			return $tags;
		};
		$attribute = new AttributeFromTag($optionsBuilder, $product, $tagType);
		$this->attributes[$attribute->getIdentificator()] = $attribute;
		return $attribute;
	}


	/**
	 * @param ActiveProduct $product
	 * @return ProductAttribute
	 */
	public function createNewProductAttribute(ActiveProduct $product) {
		$row = $this->productAttributeTable->insert(['product_id' => $product->getId()]);
		$attribute = new ProductAttribute($this->productAttributeOptionsBuilder, $product, $row->id, []);
		$product->addAttribute($attribute);
		return $this->buildProductAttributeDependencies($attribute);
	}


	/**
	 * @param ProductAttribute $attribute
	 * @return ProductAttributeOption[]
	 */
	public function productAttributeOptionsBuilder(ProductAttribute $attribute) {
		$options = [];
		foreach ($this->optionTable->findBy('product_attribute_id', $attribute->getId()) as $optionRow) {
			$option = $this->attributeOptionFactory->createProductAttributeOption($optionRow, $attribute);
			$options[$option->getIdentificator()] = $option;
		}
		return $options;
	}


	/**
	 * @param ProductAttribute $attribute
	 * @return ProductAttribute
	 */
	protected function buildProductAttributeDependencies(ProductAttribute $attribute) {
		$attribute->setDependencies($this->productAttributeTextTable);
		$this->attributes[$attribute->getIdentificator()] = $attribute;
		return $attribute;
	}

}