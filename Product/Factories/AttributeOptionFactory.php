<?php

namespace App\Model\Data\Product;

use App\Model\Data\BaseFactory;
use App\Model\Data\Tag;
use App\Model\Database\Table\ProductAttributeOptionPriceTable;
use App\Model\Database\Table\ProductAttributeOptionTable;
use App\Model\Database\Table\ProductAttributeOptionTextTable;
use App\Model\Database\Table\ProductHasTagTable;
use App\Model\Database\Table\TagTable;
use Nette\Database\Table\IRow;


/**
 * AttributeOptionFactory
 *
 * @author Kryštof Česal
 */
class AttributeOptionFactory extends BaseFactory {

	/** @var ProductAttributeOptionTable */
	protected $productAttributeOptionTable;

	/** @var ProductHasTagTable */
	protected $productHasTagTable;

	/** @var TagTable */
	protected $tagTable;

	/** @var AttributeOption */
	protected $options = [];

	/** @var ProductAttributeOptionTextTable */
	protected $productAttributeOptionTextTable;

	/** @var ProductAttributeOptionPriceTable */
	protected $productAttributeOptionPriceTable;


	/**
	 * @param ProductAttributeOptionTable $productAttributeOption
	 * @param TagTable $tagTable
	 * @param ProductHasTagTable $productHasTagTable
	 * @param ProductAttributeOptionTextTable $optionTextTable
	 * @param ProductAttributeOptionPriceTable $optionPriceTable
	 */
	function __construct(ProductAttributeOptionTable $productAttributeOption, TagTable $tagTable, ProductHasTagTable $productHasTagTable, ProductAttributeOptionTextTable $optionTextTable, ProductAttributeOptionPriceTable $optionPriceTable) {
		$this->productAttributeOptionTable = $productAttributeOption;
		$this->tagTable = $tagTable;
		$this->productHasTagTable = $productHasTagTable;
		$this->productAttributeOptionPriceTable = $optionPriceTable;
		$this->productAttributeOptionTextTable = $optionTextTable;
	}


	/**
	 * @param int|IRow $idOrRow
	 * @param ProductAttribute $attribute
	 * @return ProductAttributeOption
	 */
	public function createProductAttributeOption($idOrRow, ProductAttribute $attribute) {
		$id = (string) $idOrRow;
		if (isset($this->options[$id])) {
			return $this->options[$id];
		}
		$row = $this->getRow($idOrRow, $this->productAttributeOptionTable);
		$names = $row->related('product_attribute_option_text')->fetchPairs('language_id', 'name');
		$prices = $row->related('product_attribute_option_price')->fetchPairs('currency_id', 'price');
		$attributeOption = new ProductAttributeOption($attribute, $row->id, $names, $prices);
		return $this->buildProductAttributeOptionDependencies($attributeOption);
	}


	/**
	 * @param ProductAttribute $attribute
	 * @return ProductAttributeOption
	 */
	public function createNewProductAttributeOption(ProductAttribute $attribute) {
		$row = $this->productAttributeOptionTable->insert(['product_attribute_id' => $attribute->getId()]);
		$attributeOption = new ProductAttributeOption($attribute, $row->id, [], []);
		$attribute->addOption($attributeOption);
		return $this->buildProductAttributeOptionDependencies($attributeOption);
	}


	/**
	 * @param ProductAttributeOption $option
	 * @return ProductAttributeOption
	 */
	protected function buildProductAttributeOptionDependencies(ProductAttributeOption $option) {
		$this->options[$option->getIdentificator()] = $option;
		$option->setDependencies($this->productAttributeOptionTextTable, $this->productAttributeOptionPriceTable, $this->productAttributeOptionTable);
		return $option;
	}


	/**
	 * @param Tag $tag
	 * @param AttributeFromTag $attribute
	 * @return AttributeOptionFromTag
	 */
	public function createAttributeOptionFromTag(Tag $tag, AttributeFromTag $attribute) {
		$identificator = AttributeOptionFromTag::createIdentificator($attribute, $tag);
		if (isset($this->options[$identificator])) {
			return $this->options[$identificator];
		}
		$attributeOption = new AttributeOptionFromTag($attribute, $tag);
		$attributeOption->setDependencies($this->productHasTagTable);
		$this->options[$identificator] = $attributeOption;
		return $attributeOption;
	}

}