<?php

namespace App\model\Data\Product;

use Nette\Utils\Image;
use Nette\Utils\Random;


/**
 * ImageProductManager
 *
 * @author Kryštof Česal
 */
class ProductImageManager {

	/** @var string */
	protected $imageDir;

	/** @var string */
	protected $wwwDir;


	/**
	 * ImageProductManager constructor.
	 * @param string $imagePath
	 * @param string $wwwDir
	 */
	public function __construct($imagePath, $wwwDir) {
		$this->imageDir = $imagePath;
		$this->wwwDir = $wwwDir;
	}


	/**
	 * @param Image $image
	 * @return string
	 */
	public function saveImage(Image $image) {
		$image->save($path = $this->imageDir . '/' . Random::generate() . '.jpg', 90);
		return $path;
	}


	/**
	 * @param $filePath
	 * @return string
	 */
	public function getUrlPath($filePath) {
		return str_replace(realpath($this->wwwDir), '', realpath($filePath));
	}

}