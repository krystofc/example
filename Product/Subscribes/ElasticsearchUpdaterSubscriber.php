<?php

namespace App\Model\Data\Product;

use App\Model\Data\Product\Events\ProductEvent;
use App\Model\Data\Product\Searching\ElasticsearchIndexer;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;


/**
 * ElasticsearchUpdaterSubscriber
 *
 * @author Kryštof Česal
 */
class ElasticsearchUpdaterSubscriber implements EventSubscriberInterface {

	protected $toIndex = [];

	/** @var ElasticsearchIndexer */
	protected $indexer;


	/**
	 * ElasticsearchUpdaterSubscriber constructor.
	 * @param ElasticsearchIndexer $indexer
	 */
	public function __construct(ElasticsearchIndexer $indexer) {
		$this->indexer = $indexer;
	}


	/**
	 * @param ProductEvent $productEvent
	 */
	public function onProductChanged(ProductEvent $productEvent) {
		$this->toIndex[$productEvent->getProduct()->getId()] = $productEvent->getProduct();
	}


	function __destruct() {
		foreach ($this->toIndex as $product) {
			$this->indexer->index($product);
		}
	}


	/**
	 * @return array The event names to listen to
	 */
	public static function getSubscribedEvents() {
		return [
			'product.updated' => 'onProductChanged',
			'product.created' => 'onProductChanged',
		];
	}
}