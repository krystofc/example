<?php

namespace App\Model\Data\Product;

use App\Model\Data\Product\Events\ProductEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;


/**
 * CacheUpdaterSubscriber
 *
 * @author Kryštof Česal
 */
class CacheUpdaterSubscriber implements EventSubscriberInterface {

	/**
	 * @param ProductEvent $productEvent
	 */
	public function onProductChanged(ProductEvent $productEvent) {
		// todo: implement it
	}


	/**
	 * @return array
	 */
	public static function getSubscribedEvents() {
		return [
			'product.updated' => 'onProductChanged',
			'product.created' => 'onProductChanged',
		];
	}

}