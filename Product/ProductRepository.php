<?php

namespace App\Model\Data; // todo: move to App\Model\Data\Product

use App\Model\Data\Product\ActiveProduct;
use App\Model\Data\Product\ProductFactory;
use App\Model\Data\Product\Searching\IProvider;
use App\Model\Data\Product\Searching\Query;
use App\Model\Data\Product\Searching\Result;
use App\Model\Database\Table\ProductTable;
use Nette\Database\Table\Selection;


/**
 * ProductRepository
 *
 * @author Kryštof Česal
 */
class ProductRepository {

	/** @var ProductTable @deprecated */
	protected $productTable;

	/** @var ProductFactory */
	protected $productFactory;

	/** @var IProvider */
	protected $searchProvider;


	/**
	 * @param ProductTable $productTable
	 * @param ProductFactory $productFactory
	 * @param IProvider $searchProvider
	 */
	function __construct(ProductTable $productTable, ProductFactory $productFactory, IProvider $searchProvider) {
		$this->productTable = $productTable;
		$this->productFactory = $productFactory;
		$this->searchProvider = $searchProvider;
	}


	/**
	 * @param $id
	 * @return ActiveProduct|null
	 */
	public function getProduct($id) {
		return $this->productFactory->create($id);
	}


	/**
	 * @param Query $query
	 * @return Result[]
	 */
	public function searchProducts(Query $query) {
		$result = $this->searchProvider->search($query);
		$products = [];
		foreach ($result->getProductIds() as $id) {
			$products[$id] = $this->getProduct($id);
		}
		$result->setProducts($products);
		return $result;
	}


	/**
	 * @param $conditions
	 * @return ActiveProduct[]
	 * @deprecated use SearchProducts
	 */
	public function findProductsBy(...$conditions) {
		return $this->getProductsFromSelection($this->productTable->findBy(...$conditions));

	}


	/**
	 * @param Selection $selection
	 * @return ActiveProduct[]
	 * @deprecated use SearchProducts
	 */
	public function getProductsFromSelection(Selection $selection) {
		$products = [];
		foreach ($selection as $productRow) {
			$products[$productRow->id] = $this->getProduct($productRow);
		}
		return $products;
	}

}