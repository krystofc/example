<?php

namespace App\Model\Data\Product;

use App\Model\Data\Currency\Currency;
use App\Model\Data\Language\Language;
use App\Model\Data\Tag;
use App\Model\Database\Table\ProductAttributeOptionPriceTable;
use App\Model\Database\Table\ProductAttributeOptionTable;
use App\Model\Database\Table\ProductAttributeOptionTextTable;
use Nette\InvalidStateException;


/**
 * ProductAttributeOption
 *
 * @author Kryštof Česal
 */
class ProductAttributeOption extends AttributeOption {

	/** @var array */
	protected $prices = [];

	/** @var array */
	protected $names = [];

	/** @var int */
	protected $id;

	/** @var ProductAttributeOptionPriceTable */
	protected $pricesTable;

	/** @var ProductAttributeOptionTextTable */
	protected $textTable;

	/** @var ProductAttributeOptionTable */
	protected $optionTable;


	/**
	 * @param Attribute $attribute
	 * @param $id
	 * @param array $names
	 * @param array $prices
	 */
	function __construct(Attribute $attribute, $id, array $names, array $prices) {
		$this->prices = $prices;
		$this->names = $names;
		$this->id = $id;
		parent::__construct($attribute);
	}


	/**
	 * @param ProductAttributeOptionTextTable $textTable
	 * @param ProductAttributeOptionPriceTable $priceTable
	 * @param ProductAttributeOptionTable $optionTable
	 */
	public function setDependencies(ProductAttributeOptionTextTable $textTable, ProductAttributeOptionPriceTable $priceTable, ProductAttributeOptionTable $optionTable) {
		$this->textTable = $textTable;
		$this->pricesTable = $priceTable;
		$this->optionTable = $optionTable;
	}


	/**
	 * @param Language $language
	 * @return string
	 */
	public function getName(Language $language) {
		if ($this->names && is_array($this->names)) {
			return isset($this->names[$language->getId()]) ? $this->names[$language->getId()] : array_values($this->names)[0];
		} else {
			return 'id - ' . $this->getId();
		}
	}


	/**
	 * @param Currency $currency
	 * @return int
	 */
	public function getPrice(Currency $currency) {
		if (isset($this->prices[$currency->getId()])) {
			return $this->prices[$currency->getId()];
		} else {
			throw new InvalidStateException("Price doesn't set");
		}
	}


	/**
	 * @param array $names $languageId => $name
	 */
	public function updateNames($names) {
		if(!$this->textTable) {
			throw new InvalidStateException("ProductAttributeOptionText table doesn't set");
		}
		foreach ($names as $languageId => $name) {
			if (isset($this->names[$languageId])) {
				if ($this->names[$languageId] != $name) {
					$this->textTable->findBy('language_id = ? AND product_attribute_option_id = ?', $languageId, $this->getId())
						->update(['name' => $name]);
				}
			} else {
				$this->textTable->insert([
					'product_attribute_option_id' => $this->getId(),
					'language_id' => $languageId,
					'name' => $name,
				]);
			}
			$this->names[$languageId] = $name;
		}
	}


	/**
	 * @param array $prices $currencyId => $price
	 */
	public function updatePrices(array $prices) {
		if(!$this->pricesTable) {
			throw new InvalidStateException("ProductAttributeOptionPrice table doesn't set");
		}
		foreach ($prices as $currencyId => $price) {
			if(isset($this->prices[$currencyId])) {
				if($this->prices[$currencyId] != $price) {
					$this->pricesTable->findBy('currency_id = ? AND product_attribute_option_id = ?', $currencyId, $this->getId())
						->update(['price' => (int)$price]);
				}
			} else {
				$this->pricesTable->insert([
					'currency_id' => $currencyId,
					'product_attribute_option_id' => $this->getId(),
					'price' => (int)$price,
				]);
			}
			$this->prices[$currencyId] = $price;
		}
	}


	/**
	 * @return string
	 */
	public function getIdentificator() {
		return (string) $this->getId();
	}


	/**
	 * @return int
	 */
	public function getId() {
		return $this->id;
	}


	/**
	 * @return array
	 */
	public function getPrices() {
		return $this->prices;
	}


	/**
	 * @return array
	 */
	public function getNames() {
		return $this->names;
	}


	/**
	 * @return void
	 */
	public function delete() {
		$this->optionTable->get($this->getId())->delete();
		$this->getAttribute()->unsetOption($this->getId());
	}
}