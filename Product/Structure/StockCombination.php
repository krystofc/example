<?php

namespace App\Model\Data\Product;

use App\Model\Database\Table\StockHasProductAttributeOptionTable;
use App\Model\Database\Table\StockHasTagTable;
use App\Model\Database\Table\StockTable;
use Nette\InvalidStateException;


/**
 * StockCombination
 *
 * @author Kryštof Česal
 */
class StockCombination {

	/** @var int */
	protected $quantity = 0;

	/** @var AttributeOption[] */
	protected $attributeOptions = [];

	/** @var string */
	protected $identificator;

	/** @var Stock */
	protected $stock;

	/** @var StockTable */
	protected $stockTable;

	/** @var StockHasProductAttributeOptionTable */
	protected $stockHasAttributeTable;

	/** @var StockHasTagTable */
	protected $stockHasTagTable;


	/**
	 * StockCombination constructor.
	 * @param AttributeOption[] $attributeOptions
	 * @param int $quantity
	 * @param Stock $stock
	 */
	public function __construct(array $attributeOptions, $quantity, Stock $stock) {
		$this->quantity = $quantity;
		$this->attributeOptions = $attributeOptions;
		$this->identificator = self::createIdentificator($attributeOptions);
		$this->stock = $stock;
	}


	/**
	 * @param StockTable $stockTable
	 * @param StockHasProductAttributeOptionTable $stockHasProductAttributeOptionTable
	 * @param StockHasTagTable $stockHasTagTable
	 */
	public function setDependencies(StockTable $stockTable, StockHasProductAttributeOptionTable $stockHasProductAttributeOptionTable, StockHasTagTable $stockHasTagTable) {
		$this->stockTable = $stockTable;
		$this->stockHasAttributeTable = $stockHasProductAttributeOptionTable;
		$this->stockHasTagTable = $stockHasTagTable;
	}


	/**
	 * @return int
	 */
	public function getQuantity() {
		return $this->quantity;
	}


	/**
	 * @param AttributeOption[] $filter
	 * @return bool
	 */
	public function isCorrespondingFilter($filter = []) {
		$optionsIdentificators = [];
		foreach ($this->attributeOptions as $option) {
			$optionsIdentificators[] = $option->getIdentificator();
		}
		foreach ($filter as $filterOption) {
			if (!in_array($filterOption->getIdentificator(), $optionsIdentificators)) {
				return false;
			}
		}
		return true;
	}


	/**
	 * @return string
	 */
	public function getIdentificator() {
		return $this->identificator;
	}


	/**
	 * @param AttributeOption[] $attributeOptions
	 * @return string
	 */
	static function createIdentificator(array $attributeOptions) {
		$identificators = [];
		foreach ($attributeOptions as $option) {
			$identificators[] = $option->getIdentificator();
		}
		asort($identificators);
		$combination = "";
		foreach ($identificators as $identificator) {
			$combination .= $identificator;
		}
		return md5($combination);
	}


	/**
	 * @param Attribute $attribute
	 * @return AttributeOption|null
	 */
	public function getSelectedOption(Attribute $attribute) {
		foreach ($this->attributeOptions as $option) {
			if ($option->getAttribute()->getIdentificator() == $attribute->getIdentificator()) {
				return $option;
			}
		}
		return NULL;
	}


	/**
	 * @param $quantity
	 * @throws \Exception
	 */
	public function update($quantity) {
		if (!$this->stockTable) {
			throw new InvalidStateException("Stock table doesn't set");
		}
		$row = $this->stockTable->findBy('identificator', $this->getIdentificator())->fetch();
		if ($row) {
			$row->update(['pieces' => (int)$quantity]);
		} else {
			$this->stockTable->begin();
			try {
				$row = $this->stockTable->insert([
					'product_id' => $this->stock->getProduct()->getId(),
					'pieces' => $quantity,
					'identificator' => $this->getIdentificator()
				]);
				$this->id = $row->getPrimary();
				foreach ($this->attributeOptions as $option) {
					if ($option instanceof ProductAttributeOption) {
						$this->stockHasAttributeTable->insert([
							'product_attribute_option_id' => $option->getId(),
							'stock_id' => $this->id
						]);
					} elseif ($option instanceof AttributeOptionFromTag) {
						$this->stockHasTagTable->insert([
							'tag_id' => $option->getTag()->getId(),
							'stock_id' => $this->id
						]);
					}
				}
				$this->stockTable->commit();
			} catch (\Exception $e) {
				$this->stockTable->rollback();
				throw $e;
			}
		}
		$this->quantity = $quantity;
	}


	/**
	 * @return AttributeOption[]
	 */
	public function getAttributeOptions() {
		return $this->attributeOptions;
	}

} 