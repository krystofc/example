<?php

namespace App\Model\Data\Product;

use App\Model\Data\Currency\Currency;
use App\Model\Data\Language\Language;


/**
 * AttributeOption
 *
 * @author Kryštof Česal
 */
abstract class AttributeOption {

	/** @var Attribute */
	protected $attribute;


	/**
	 * @param Language $language
	 * @return string
	 */
	abstract public function getName(Language $language);


	/**
	 * @param Currency $currency
	 * @return int
	 */
	abstract public function getPrice(Currency $currency);


	/**
	 * @return array
	 */
	abstract public function getPrices();


	/**
	 * @return array
	 */
	abstract public function getNames();


	/**
	 * @return string
	 */
	abstract public function getIdentificator();


	/**
	 * @param Attribute $attribute
	 */
	function __construct(Attribute $attribute) {
		$this->attribute = $attribute;
	}


	/**
	 * @return Attribute
	 */
	public function getAttribute() {
		return $this->attribute;
	}

}