<?php

namespace App\Model\Data\Product;

use App\Model\Data\Category;
use App\Model\Data\Product\Events\ProductUpdatedEvent;
use App\Model\Data\Tag;
use App\Model\Database\Table\ProductHasTagTable;
use App\Model\Database\Table\ProductPriceTable;
use App\Model\Database\Table\ProductTable;
use App\Model\Database\Table\ProductTextTable;
use Nette\InvalidStateException;


/**
 * Product
 *
 * @author Kryštof Česal
 */
class ActiveProduct extends Product { // todo: events, logger

	/** @var ProductTable */
	protected $productTable;

	/** @var ProductHasTagTable */
	protected $productHasTagTable;

	/** @var ProductPriceTable */
	protected $productPriceTable;

	/** @var ProductTextTable */
	protected $productTextTable;

	/** @var AttributeFactory */
	protected $attributeFactory;

	/** @var AttributeOptionFactory */
	protected $attributeOptionFactory;

	/** @var ProductEventDispatcher */
	protected $eventDispatcher;


	/**
	 * @param ProductTable $productTable
	 * @param ProductHasTagTable $productHasTagTable
	 * @param ProductPriceTable $productPriceTable
	 * @param ProductTextTable $productTextTable
	 * @param AttributeFactory $attributeFactory
	 * @param AttributeOptionFactory $attributeOptionFactory
	 * @param ProductEventDispatcher $dispatcher
	 */
	public function setDependencies(ProductTable $productTable, ProductHasTagTable $productHasTagTable, ProductPriceTable $productPriceTable, ProductTextTable $productTextTable, AttributeFactory $attributeFactory, AttributeOptionFactory $attributeOptionFactory, ProductEventDispatcher $dispatcher) {
		$this->productTable = $productTable;
		$this->productHasTagTable = $productHasTagTable;
		$this->productPriceTable = $productPriceTable;
		$this->productTextTable = $productTextTable;
		$this->attributeFactory = $attributeFactory;
		$this->attributeOptionFactory = $attributeOptionFactory;
		$this->eventDispatcher = $dispatcher;
	}


	/**
	 * @param bool $reverse
	 */
	public function delete($reverse = false) {
		if (!$this->productTable) {
			throw new InvalidStateException("Product table doesn't set");
		}
		$this->productTable->update($this->getId(), ['deleted' => $reverse ? 0 : 1]);
		$this->flags['deleted'] = !$reverse;
		$this->eventDispatcher->dispatch('product.updated', new ProductUpdatedEvent($this));
	}


	/**
	 * @param $data
	 */
	public function update($data) {
		if (!$this->productTable) {
			throw new InvalidStateException("Product table doesn't set");
		}
		$data = array_intersect_key(($data), array_flip(['tailored', 'after_sold_out',
														 'production_time', 'image',
														 'commission', 'visible', 'gender']));
		$changes = [];
		foreach ($this->flags as $flagName => $actualValue) {
			if (isset($data[$flagName]) && $actualValue != $data[$flagName]) {
				$this->flags[$flagName] = $changes[$flagName] = $data[$flagName];
			}
		}
		if (isset($data['gender']) && $this->gender != $data['gender']) {
			$this->gender = $changes['gender'] = $data['gender'];
		}
		if (isset($data['image']) && $this->image != $data['image']) {
			$this->image = $changes['image'] = $data['image'];
		}
		if (isset($data['commission']) && $this->commissionRatio != $data['commission']) {
			$this->commissionRatio = $changes['commission'] = $data['commission'];
		}
		if (isset($data['production_time']) && $this->productionTime != $data['production_time']) {
			$this->productionTime = $changes['production_time'] = $data['production_time'];
		}
		if ($changes) {
			$this->productTable->update($this->getId(), $changes);
			$this->eventDispatcher->dispatch('product.updated', new ProductUpdatedEvent($this));
		}
	}


	/**
	 * @param Category $category
	 */
	public function changeCategory(Category $category) {
		if (!$this->productTable) {
			throw new InvalidStateException("Product table doesn't set");
		}
		if ($this->getCategory()->getId() !== $category->getId()) {
			$this->categoryId = $category->getId();
			$this->category = $category;
			$this->productTable->update($this->getId(), ['category_id' => $category->getId()]);
			$this->eventDispatcher->dispatch('product.updated', new ProductUpdatedEvent($this));
		}
	}


	/**
	 * @param Tag[] $newTags
	 * @throws \Exception
	 */
	public function setTags(array $newTags) {
		if (!$this->productHasTagTable) {
			throw new InvalidStateException("ProductHasTag table doesn't set");
		}
		$this->productHasTagTable->begin();
		try {
			$updated = false;
			$newTagsById = [];
			foreach ($newTags as $tag) {
				$newTagsById[$tag->getId()] = $tag;
			}
			$newTags = $newTagsById;
			$this->getAttributes(); // build attributes
			foreach ($this->getTags() as $actualTagId => $actualTag) {
				if (!in_array($actualTagId, array_keys($newTags))) {
					$this->productHasTagTable->findBy(['product_id' => $this->getId(), 'tag_id' => $actualTag->getId()])
						->delete();
					if ($actualTag->getTagType()->isSelectable()) {
						$attributeId = AttributeFromTag::createIdentificator($this, $actualTag->getTagType());
						$this->attributes[$attributeId]->unsetOption($actualTag->getId());
						if (!$this->attributes[$attributeId]->getOptions()) {
							unset($this->attributes[$attributeId]);
						}
					}
					unset($this->tags[$actualTag->getId()]);
					$updated = true;
				}
			}
			$tagToNewAttributes = [];
			/**
			 * @var int $newTagId
			 * @var Tag $newTag
			 */
			foreach ($newTags as $newTagId => $newTag) {
				if (!in_array($newTagId, array_keys($this->tags))) {
					$this->productHasTagTable->insert(['product_id' => $this->getId(), 'tag_id' => $newTagId]);
					$this->tags[$newTagId] = $newTag;
					if ($newTag->getTagType()->isSelectable()) {
						if (!isset($tagToNewAttributes[$newTag->getTagType()->getId()])) {
							$tagToNewAttributes[$newTag->getTagType()->getId()] = [];
						}
						$tagToNewAttributes[$newTag->getTagType()->getId()][$newTag->getId()] = $newTag;
					}
				}
			}
			foreach ($tagToNewAttributes as $tagTypeId => $tags) {
				/** @var Tag $tag */
				$tag = array_values($tags)[0];
				$identificator = AttributeFromTag::createIdentificator($this, $tag->getTagType());
				if (isset($this->attributes[$identificator])) {
					foreach ($tags as $tag) {
						$option = $this->attributeOptionFactory->createAttributeOptionFromTag($tag, $this->attributes[$identificator]);
						$this->attributes[$identificator]->addOption($option);
					}
				} else {
					$this->attributes[$identificator] = $this->attributeFactory->createAttributeFromTag($tag->getTagType(), array_keys($tags), $this);
				}
				$updated = true;
			}
			if ($updated) {
				$this->eventDispatcher->dispatch('product.updated', new ProductUpdatedEvent($this));
			}
		} catch (\Exception $e) {
			$this->productHasTagTable->rollback();
			throw $e;
		}
		$this->productHasTagTable->commit();
	}


	/**
	 * @param array $prices $currencyId => $amount
	 */
	public function updatePrices($prices) {
		$this->productPriceTable->begin();
		try {
			$updated = false;
			foreach ($prices as $currencyId => $price) {
				if (isset($this->prices[$currencyId])) {
					if ($this->prices[$currencyId] != $price) {
						$this->productPriceTable->findBy('currency_id = ? AND product_id = ?', $currencyId, $this->getId())
							->update(['price' => $price]);
						$updated = true;
					}
				} else {
					$this->productPriceTable->insert(['currency_id' => $currencyId,
													  'product_id' => $this->getId(),
													  'price' => $price]);
					$updated = true;
				}
				$this->prices[$currencyId] = $price;
			}
			if ($updated) {
				$this->eventDispatcher->dispatch('product.updated', new ProductUpdatedEvent($this));
			}
		} catch (\Exception $e) {
			$this->productPriceTable->rollback();
		}
		$this->productPriceTable->commit();
	}


	/**
	 * @param array $texts $languageId => [name => $name]
	 */
	public function updateTexts($texts) {
		$this->productTextTable->begin();
		try {
			$updated = false;
			foreach ($texts as $languageId => $text) {
				if (isset($this->texts[$languageId])) {
					if ($this->texts[$languageId] != $text) {
						$this->productTextTable->findBy('product_id = ? AND language_id = ?', $this->getId(), $languageId)
							->update(['name' => $text['name'], 'about' => $text['about']]);
						$updated = true;
					}
				} else {
					$this->productTextTable->insert([
						'product_id' => $this->getId(),
						'language_id' => $languageId,
						'name' => $text['name'],
						'about' => $text['about'],
					]);
					$updated = true;
				}
				$this->texts[$languageId] = $text;
			}
			if ($updated) {
				$this->eventDispatcher->dispatch('product.updated', new ProductUpdatedEvent($this));
			}
		} catch (\Exception $e) {
			$this->productTextTable->rollback();
		}
		$this->productTextTable->commit();
	}


	/**
	 * @param ProductAttribute $attribute
	 */
	public function addAttribute(ProductAttribute $attribute) {
		$this->getAttributes();
		$this->attributes[$attribute->getIdentificator()] = $attribute;
	}


	/**
	 * @param ProductAttribute $attribute
	 */
	public function deleteAttribute(ProductAttribute $attribute) {
		unset($this->attributes[$attribute->getIdentificator()]);
		// todo: implement it
	}

}