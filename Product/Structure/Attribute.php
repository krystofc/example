<?php

namespace App\Model\Data\Product;

use App\Model\Data\Language\Language;


/**
 * Attribute
 *
 * @author Kryštof Česal
 */
abstract class Attribute {

	const TYPE_CATEGORY = 1;
	const TYPE_PRODUCT = 2;

	/** @var int self::TYPE_CATEGORY|self::TYPE_PRODUCT */
	protected $type;

	/** @var ActiveProduct */
	protected $product;

	/** @var bool */
	protected $required; // todo: What's the functionality??

	/** @var AttributeOption[] */
	protected $options;

	/** @var callable */
	protected $optionsBuilder;


	/**
	 * @param Language $language
	 * @return string
	 */
	abstract function getName(Language $language);


	/**
	 * @return string
	 */
	abstract function getIdentificator();


	/**
	 * @param callable $optionsBuilder
	 * @param ActiveProduct $product
	 */
	function __construct(callable $optionsBuilder, ActiveProduct $product) {
		$this->optionsBuilder = $optionsBuilder;
		$this->product = $product;
	}


	/**
	 * @return AttributeOption[]
	 */
	public function getOptions() {
		if ($this->optionsBuilder) {
			$this->options = call_user_func($this->optionsBuilder, $this);
			$this->optionsBuilder = false;
		}
		return $this->options;
	}


	/**
	 * @return boolean
	 */
	public function isRequired() {
		return $this->required;
	}


	/**
	 * @return int
	 */
	public function getType() {
		return $this->type;
	}


	/**
	 * @return ActiveProduct
	 */
	public function getProduct() {
		return $this->product;
	}


	/**
	 * @param $id
	 */
	public function unsetOption($id) {
		$this->getOptions();
		unset($this->options[$id]);
		// todo: event
	}


	public function addOption(AttributeOption $attributeOption) {
		$this->getOptions(); // build
		$this->options[$attributeOption->getIdentificator()] = $attributeOption;
	}


	/**
	 * @param $optionIdentificator
	 * @param bool $need
	 * @return AttributeOption|null
	 */
	public function getOption($optionIdentificator, $need = false) {
		$this->getOptions(); // build...
		if (isset($this->options[$optionIdentificator])) {
			return $this->options[$optionIdentificator];
		} elseif ($need) {
			throw new \InvalidArgumentException("Option with identificator '{$optionIdentificator}' doesn't exist");
		}
		return NULL;
	}

}