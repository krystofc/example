<?php

namespace App\Model\Data\Product;

use App\Model\Data\Currency\Currency;
use App\Model\Data\Language\Language;
use App\Model\Data\Tag;
use App\Model\Database\Table\ProductHasTagTable;
use Nette\InvalidStateException;


/**
 * AttributeOptionFromTag
 *
 * @author Kryštof Česal
 */
class AttributeOptionFromTag extends AttributeOption {

	/** @var AttributeFromTag */
	protected $attribute;

	/** @var Tag */
	protected $tag;

	/** @var ProductHasTagTable */
	protected $productHasTagTable;


	/**
	 * @param AttributeFromTag $attribute
	 * @param Tag $tag
	 */
	function __construct(AttributeFromTag $attribute, Tag $tag) {
		$this->tag = $tag;
		parent::__construct($attribute);
	}


	/**
	 * @param ProductHasTagTable $productHasTagTable
	 */
	public function setDependencies(ProductHasTagTable $productHasTagTable) {
		$this->productHasTagTable = $productHasTagTable;
	}


	/**
	 * @param Language $language
	 * @return string
	 */
	public function getName(Language $language) {
		return $this->tag->getName($language);
	}


	/**
	 * @param Currency $currency
	 * @return int
	 */
	public function getPrice(Currency $currency) {
		return 0;
	}


	/**
	 * @return void
	 */
	public function delete() {
		if (!$this->productHasTagTable) {
			throw new InvalidStateException("ProductHasTag table doesn't set");
		}
		$this->productHasTagTable->findBy('tag_id = ? AND product_id = ?', $this->getId(), $this->attribute->getProduct()
			->getId())->delete();
		$this->attribute->unsetOption($this->getId());
	}


	/**
	 * @return string
	 */
	public function getIdentificator() {
		return self::createIdentificator($this->attribute, $this->tag);
	}


	static public function createIdentificator(AttributeFromTag $attribute, Tag $tag) {
		return $attribute->getIdentificator() . '-' . $tag->getId();
	}


	/**
	 * @return array
	 */
	public function getPrices() {
		return [];
	}


	/**
	 * @return array
	 */
	public function getNames() {
		return $this->tag->getNames();
	}


	/**
	 * @return Tag
	 */
	public function getTag() {
		return $this->tag;
	}

}