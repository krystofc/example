<?php

namespace App\Model\Data\Product;

use App\Model\Cache\ICachedEntity;
use App\Model\Data\Category;
use App\Model\Data\Currency\Currency;
use App\Model\Data\Language\Language;
use App\Model\Data\Tag;
use Nette\InvalidStateException;

/**
 * Product
 *
 * @author Kryštof Česal
 */
class Product implements ICachedEntity {

	/** @var int */
	protected $id;

	/** @var string */
	protected $texts;

	/** @var string */
	protected $about;

	/** @var int */
	protected $prices;

	/** @var int */
	protected $productionTime;

	/** @var Attribute[] */
	protected $attributes = NULL;

	/** @var Tag[] */
	protected $tags = NULL;

	/** @var callable */
	protected $tagsBuilder;

	/** @var array */
	protected $tagsIds = NULL;

	/** @var array */
	protected $attributesIds = NULL;

	/** @var callable */
	protected $attributesBuilder;

	/** @var int */
	protected $sellerId; // todo: sellersRepo

	/** @var int */
	protected $commissionRatio;

	/** @var Category */
	protected $category;

	/** @var int */
	protected $categoryId;

	/** @var callable|false */
	protected $categoryBuilder;

	/** @var Stock */
	protected $stock;

	/** @var callable|false */
	protected $stockBuilder;

	/** @var int */
	protected $gender;

	/** @var string */
	protected $image;


	/** @var array */
	protected $flags = [
		'tailored' => NULL,
		'deleted' => NULL,
		'visible' => NULL,
		'after_sold_out' => NULL,
	];


	/**
	 * @param int $id
	 * @param array $texts
	 * @param array $price
	 * @param $productionTime
	 * @param bool $tailored
	 * @param bool $deleted
	 * @param bool $visible
	 * @param int $sellerId
	 * @param int $commissionRatio
	 * @param bool $afterSoldOut
	 * @param int $categoryId
	 * @param int $gender
	 * @param $image
	 */
	function __construct($id, array $texts, array $price, $productionTime, $tailored, $deleted, $visible, $sellerId, $commissionRatio, $afterSoldOut, $categoryId, $gender, $image) {
		$this->id = $id;
		$this->texts = $texts;
		$this->prices = $price;
		$this->sellerId = $sellerId;
		$this->commissionRatio = $commissionRatio;
		$this->productionTime = $productionTime;
		$this->flags = [
			'tailored' => $tailored,
			'deleted' => $deleted,
			'visible' => $visible,
			'after_sold_out' => $afterSoldOut,
		];
		$this->categoryId = $categoryId;
		$this->gender = $gender;
		$this->image = $image;
	}


	/**
	 * @return mixed
	 */
	public function getId() {
		return $this->id;
	}


	/**
	 * @param Language $language
	 * @return string
	 */
	public function getName(Language $language) {
		return isset($this->texts[$language->getId()]['name']) ? $this->texts[$language->getId()]['name'] : ($this->texts ? array_values($this->texts)[0]['names'] : '');
	}


	/**
	 * @param Currency $currency
	 * @param bool $needed
	 * @return int
	 */
	public function getPrice(Currency $currency, $needed = true) {
		if (isset($this->prices[$currency->getId()])) {
			return $this->prices[$currency->getId()];
		} elseif ($needed) {
			throw new InvalidStateException("Price in '{$currency->getName()}' currency doesn't set");
		} else {
			return NULL;
		}
	}


	/**
	 * @return boolean
	 */
	public function isTailored() {
		return $this->flags['tailored'];
	}


	/**
	 * @return int
	 */
	public function getSellerId() {
		return $this->sellerId;
	}


	/**
	 * @return int
	 */
	public function getCommissionRatio() {
		return $this->commissionRatio;
	}


	/**
	 * @return string
	 */
	public function getImage() {
		return $this->image;
	}


	/**
	 * @return Category
	 */
	public function getCategory() {
		if ($this->categoryBuilder) {
			$this->category = call_user_func($this->categoryBuilder, $this);
			$this->categoryBuilder = false;
		}
		return $this->category;
	}


	/**
	 * @return Attribute[]
	 */
	public function getAttributes() {
		if ($this->attributesBuilder) {
			$this->attributes = call_user_func($this->attributesBuilder, $this);
			$this->attributesBuilder = false;
		}
		return $this->attributes;
	}


	/**
	 * @return Tag[]
	 */
	public function getTags() {
		if ($this->tagsBuilder) {
			$this->tags = call_user_func($this->tagsBuilder, $this);
			$this->tagsBuilder = false;
		}
		return $this->tags;
	}


	/**
	 * @return Stock
	 */
	public function getStock() {
		if ($this->stockBuilder) {
			$this->stock = call_user_func($this->stockBuilder, $this);
			$this->stockBuilder = false;
		}
		return $this->stock;
	}


	/**
	 * @return int
	 */
	public function getCategoryId() {
		return $this->categoryId;
	}


	/**
	 * @return bool
	 */
	public function isVisible() {
		return $this->flags['visible'];
	}


	/**
	 * @return bool
	 */
	public function isAfterSoldOut() {
		return $this->flags['after_sold_out'];
	}


	/**
	 * @return bool
	 */
	public function isDeleted() {
		return $this->flags['deleted'];
	}


	/**
	 * @return int
	 */
	public function getProductionTime() {
		return $this->productionTime;
	}


	/**
	 * @return int
	 */
	public function getGender() {
		$this->gender;
	}


	/**
	 * @param callable $categoryBuilder
	 * @param callable $attributesBuilder
	 * @param callable $tagsBuilder
	 * @param callable $stockBuilder
	 */
	public function setBuilders(callable $categoryBuilder, callable $attributesBuilder, callable $tagsBuilder, callable $stockBuilder) {
		$this->categoryBuilder = $categoryBuilder;
		$this->attributesBuilder = $attributesBuilder;
		$this->tagsBuilder = $tagsBuilder;
		$this->stockBuilder = $stockBuilder;
	}


	/**
	 * @return array
	 */
	public function toFormArray() {
		$values = $this->flags;
		$values['category_id'] = $this->getCategory()->getId();
		$values['production_time'] = $this->productionTime;
		$values['translates'] = [];
		$values['translates'] = $this->texts;
		$values['currencies'] = $this->prices;
		$values['gender'] = $this->gender;
		$values['tags'] = [];
		foreach ($this->getTags() as $tag) {
			if (!isset($values['tags'][$tag->getTagType()->getId()])) {
				$values['tags'][$tag->getTagType()->getId()] = [];
			}
			$values['tags'][$tag->getTagType()->getId()][] = $tag->getId();
		}
		$values['attributes'] = [];
		foreach ($this->getAttributes() as $attribute) {
			if ($attribute instanceof ProductAttribute) {
				$attributeArray = ['names' => $attribute->getNames(), 'options' => []];
				foreach ($attribute->getOptions() as $option) {
					$attributeArray['options']['u' . $option->getIdentificator()] = ['names' => $option->getNames(),
																					 'prices' => $option->getPrices()];
				}
				$values['attributes'] += ['u' . $attribute->getId() => $attributeArray];
			}
		}
		return $values;
	}


	/**
	 * @return array
	 */
	public function getTagsIds() {
		return $this->tagsIds;
	}


	/**
	 * @return array
	 */
	function __sleep() {
		if (is_array($this->attributes)) {
			$this->attributesIds = [Attribute::TYPE_CATEGORY => [], Attribute::TYPE_PRODUCT => []];
			foreach ($this->attributes as $attribute) {
				$this->attributesIds[$attribute->getType()][] = $attribute->getIdentificator();
			}
		}
		return ['id', 'texts', 'prices', 'attributesIds', 'sellerId', 'commissionRatio', 'categoryId', 'flags',
				'productionTime'];
	}


	/**
	 * @return string
	 */
	function __toString() {
		return '' . $this->getId();
	}

}