<?php

namespace App\Model\Data\Product;


/**
 * Stock
 *
 * @author Kryštof Česal
 */
class Stock {

	/** @var StockCombination[] */
	protected $combinations = [];

	/** @var callable */
	protected $combinationsBuilder;

	/** @var Product */
	protected $product;


	/**
	 * Stock constructor.
	 * @param Product $product
	 * @param callable $combinationsBuilder
	 */
	public function __construct(Product $product, callable $combinationsBuilder) {
		$this->product = $product;
		$this->combinationsBuilder = $combinationsBuilder;
	}


	/**
	 * @param array $filter
	 * @return int
	 */
	public function getQuantity($filter = []) {
		$quantity = 0;
		foreach ($this->getCombinations($filter) as $combination) {
			$quantity += $combination->getQuantity();
		}
		return $quantity;
	}


	/**
	 * @param array $filter
	 * @return StockCombination[]
	 */
	public function getAvailableCombinations($filter = []) {
		$combinations = $this->getCombinations($filter);
		foreach ($combinations as $key => $combination) {
			if (!$combination->getQuantity()) {
				unset($combinations[$key]);
			}
		}
		return $combinations;
	}


	/**
	 * @param array $filter
	 * @return StockCombination[]
	 */
	public function getCombinations($filter = []) {
		if($this->combinationsBuilder) {
			$this->combinations = call_user_func($this->combinationsBuilder, $this);
			$this->combinationsBuilder = false;
		}
		$combinations = $this->combinations;
		if ($filter) {
			foreach ($combinations as $key => $combination) {
				if (!$combination->isCorrespondingFilter($filter)) {
					unset($combinations[$key]);
				}
			}
		}
		return $combinations;
	}


	/**
	 * @return Product
	 */
	public function getProduct() {
		return $this->product;
	}

}