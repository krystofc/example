<?php

namespace App\Model\Data\Product;

use App\Model\Data\Language\Language;
use App\Model\Database\Table\ProductAttributeTextTable;
use Nette\InvalidStateException;

/**
 * ProductAttribute
 *
 * @author Kryštof Česal
 */
class ProductAttribute extends Attribute {

	/** @var int */
	protected $id;

	/** @var array */
	protected $names = [];

	/** @var int */
	protected $type = self::TYPE_PRODUCT;

	/** @var bool */
	protected $required = true;

	/** @var ProductAttributeTextTable */
	protected $productAttributeTextTable;


	/**
	 * @param callable $optionsBuilder
	 * @param ActiveProduct $product
	 * @param int $id
	 * @param array $names
	 */
	function __construct(callable $optionsBuilder, ActiveProduct $product, $id, array $names) {
		$this->id = (int) $id;
		$this->names = $names;
		parent::__construct($optionsBuilder, $product);
	}


	/**
	 * @param ProductAttributeTextTable $productAttributeTextTable
	 */
	public function setDependencies(ProductAttributeTextTable $productAttributeTextTable) {
		$this->productAttributeTextTable = $productAttributeTextTable;
	}


	/**
	 * @param Language $language
	 * @return string
	 */
	public function getName(Language $language) {
		if ($this->names && is_array($this->names)) {
			return isset($this->names[$language->getId()]) ? $this->names[$language->getId()] : array_values($this->names)[0];
		} else {
			return 'id - ' . $this->getId();
		}
	}


	/**
	 * @param array $names $languageId => $name
	 * @return $this
	 */
	public function updateNames(array $names) {
		if (!$this->productAttributeTextTable) {
			throw new InvalidStateException("ProductAttributeText table doesn't set");
		}
		foreach ($names as $languageId => $name) {
			if (isset($this->names[$languageId])) {
				if ($this->names[$languageId] != $name) {
					$this->productAttributeTextTable->findBy([
						'language_id' => $languageId,
						'product_attribute_id' => $this->getId(),
					])->update(['name' => $name]);
					$this->names[$languageId] = $name;
				}
			} else {
				$this->productAttributeTextTable->insert([
					'product_attribute_id' => $this->getId(),
					'language_id' => $languageId,
					'name' => $name,
				]);
				$this->names[$languageId] = $name;
			}
		}
		return $this;
	}


	/**
	 * @return int
	 */
	public function getId() {
		return $this->id;
	}


	/**
	 * @return string
	 */
	function getIdentificator() {
		return (string) $this->getId();
	}


	/**
	 * @return array
	 */
	public function getNames() {
		return $this->names;
	}


	public function delete() {
		// todo: implement it!
	}

} 