<?php

namespace App\Model\Data\Product;

use App\Model\Data\Language\Language;
use App\Model\Data\TagType;


/**
 * AttributeFromTag
 *
 * @author Kryštof Česal
 */
class AttributeFromTag extends Attribute {

	protected $type = self::TYPE_CATEGORY;

	/** @var TagType */
	protected $tagType;


	/**
	 * @param callable $optionsBuilder
	 * @param ActiveProduct $product
	 * @param TagType $tagType
	 */
	function __construct(callable $optionsBuilder, ActiveProduct $product, TagType $tagType) {
		parent::__construct($optionsBuilder, $product);
		$this->tagType = $tagType;
	}


	/**
	 * @param Language $language
	 * @return string
	 */
	public function getName(Language $language) {
		return $this->tagType->getName($language);
	}


	/**
	 * @return TagType
	 */
	public function getTagType() {
		return $this->tagType;
	}


	/**
	 * @return string
	 */
	function getIdentificator() {
		return self::createIdentificator($this->product, $this->tagType);
	}


	/**
	 * @param Product $product
	 * @param TagType $tagType
	 * @return string
	 */
	static public function createIdentificator(Product $product, TagType $tagType) {
		return $product->getId() . '-' . $tagType->getId();
	}

}