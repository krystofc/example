<?php

namespace App\Model\Data\Product\Searching;

use App\Model\Data\Currency\CurrencyRepository;
use App\Model\Data\Language\LanguageRepository;
use App\Model\Data\Product\AttributeOptionFromTag;
use App\Model\Data\Product\Product;
use App\Model\Data\Product\ProductAttributeOption;
use Elastica\Document;
use Elastica\Index;
use Elastica\Type;


/**
 * ElasticSearchIndexer
 *
 * @author Kryštof Česal
 */ 
class ElasticsearchIndexer implements IIndexer {

	/** @var Type */
	protected $elasticType;

	/** @var LanguageRepository */
	protected $languageRepository;

	/** @var CurrencyRepository */
	protected $currencyRepository;


	/**
	 * ElasticsearchIndexer constructor.
	 * @param Index $index
	 * @param LanguageRepository $languageRepository
	 * @param CurrencyRepository $currencyRepository
	 */
	public function __construct(Index $index, LanguageRepository $languageRepository, CurrencyRepository $currencyRepository) {
		$this->elasticType = $index->getType('product');
		$this->languageRepository = $languageRepository;
		$this->currencyRepository = $currencyRepository;
	}


	/**
	 * @param Product $product
	 * @return bool
	 */
	public function index(Product $product) {
		$czechLang = $this->languageRepository->getAllLanguages()[1];
		$czechCrowd = $this->currencyRepository->getCurrency(1);
		$data = [
			'seller_id' => $product->getSellerId(),
			'category_id' => $product->getCategory()->getId(),
			'after_sold_out' => $product->isAfterSoldOut(),
			'tailored' => $product->isTailored(),
			'name' => $product->getName($czechLang),
			'price' => $product->getPrice($czechCrowd, false),
			'tags' => [],
			'options' => [],
		];
		foreach ($product->getTags() as $tag) {
			$data['tags'][] = $tag->getId();
		}
		foreach ($product->getStock()->getCombinations() as $combination) {
			$optionData = ['count' => $combination->getQuantity(), 'op' => []];
			foreach($combination->getAttributeOptions() as $option) {
				if($option instanceof AttributeOptionFromTag) {
					$optionData['op'][] = 't'.$option->getTag()->getId();
				} elseif($option instanceof ProductAttributeOption) {
					$optionData['op'][] = 'a'.$option->getId();
				}
			}
			$data['options'][] = $optionData;
		}
		$response = $this->elasticType->addDocument(new Document($product->getId(), $data));
		$this->elasticType->getIndex()->refresh();
	}


	/**
	 * @return \Elastica\Response
	 */
	public function drop() {
		return $this->elasticType->delete();
	}
}