<?php

namespace App\Model\Data\Product\Searching;
use App\Model\Data\Product\ActiveProduct;
use Nette\InvalidStateException;


/**
 * Result
 *
 * @author Kryštof Česal
 */
class Result implements \Iterator {

	/** @var int */
	protected $minPrice;

	/** @var int */
	protected $maxPrice;

	/** @var array */
	protected $productIds = [];

	/** @var ActiveProduct[] */
	protected $products = [];


	/**
	 * @param array $productIds
	 * @param int $maxPrice
	 * @param int $minPrice
	 */
	function __construct(array $productIds, $minPrice, $maxPrice) {
		$this->maxPrice = $maxPrice;
		$this->minPrice = $minPrice;
		$this->productIds = $productIds;
	}


	/**
	 * @return int
	 */
	public function getMaxPrice() {
		return $this->maxPrice;
	}


	/**
	 * @return int
	 */
	public function getMinPrice() {
		return $this->minPrice;
	}


	/**
	 * @return array
	 */
	public function getProductIds() {
		return $this->productIds;
	}


	/**
	 * (PHP 5 &gt;= 5.0.0)<br/>
	 * Return the current element
	 * @link http://php.net/manual/en/iterator.current.php
	 * @return mixed Can return any type.
	 */
	public function current() {
		return current($this->products);
	}


	/**
	 * (PHP 5 &gt;= 5.0.0)<br/>
	 * Move forward to next element
	 * @link http://php.net/manual/en/iterator.next.php
	 * @return void Any returned value is ignored.
	 */
	public function next() {
		next($this->products);
	}


	/**
	 * (PHP 5 &gt;= 5.0.0)<br/>
	 * Return the key of the current element
	 * @link http://php.net/manual/en/iterator.key.php
	 * @return mixed scalar on success, or null on failure.
	 */
	public function key() {
		return key($this->products);
	}


	/**
	 * (PHP 5 &gt;= 5.0.0)<br/>
	 * Checks if current position is valid
	 * @link http://php.net/manual/en/iterator.valid.php
	 * @return boolean The return value will be casted to boolean and then evaluated.
	 * Returns true on success or false on failure.
	 */
	public function valid() {
		return isset($this->products[$this->key()]);
	}


	/**
	 * (PHP 5 &gt;= 5.0.0)<br/>
	 * Rewind the Iterator to the first element
	 * @link http://php.net/manual/en/iterator.rewind.php
	 * @return void Any returned value is ignored.
	 */
	public function rewind() {
		reset($this->products);
	}


	/**
	 * @param ActiveProduct[] $products
	 */
	public function setProducts(array $products) {
		$this->products = $products;
	}


	/**
	 * @return ActiveProduct
	 */
	public function getProducts() {
		if(count($this->productIds) != count($this->products)) {
			throw new InvalidStateException("Products don't set");
		}
		return $this->products;
	}


}