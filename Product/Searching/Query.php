<?php

namespace App\Model\Data\Product\Searching;

use App\Model\Data\Category;
use App\Model\Data\Tag;


/**
 * Query
 *
 * @author Kryštof Česal
 */
class Query {

	/** @var Category[] */
	protected $categories = [];

	/** @var Tag[] */
	protected $tags = [];

	/** @var string */
	protected $fulltext = NULL;

	/** @var bool */
	protected $onlyAvailable = true;

	/** @var array */
	protected $sellerIds;

	/** @var array [from => '', to => ''] */
	protected $price;


	/**
	 * @param boolean $onlyAvailable
	 * @return $this
	 */
	public function setOnlyAvailable($onlyAvailable) {
		$this->onlyAvailable = $onlyAvailable;
		return $this;
	}


	/**
	 * @param array $sellerIds
	 * @return $this
	 */
	public function setSellerIds($sellerIds) {
		$this->sellerIds = $sellerIds;
		return $this;
	}


	/**
	 * @param Category $category
	 * @return $this
	 */
	public function addCategory(Category $category) {
		if ($category->getSubcategories()) {
			foreach ($category->getSubcategories() as $subcategory) {
				$this->addCategory($subcategory);
			}
		}
		$this->categories[$category->getId()] = $category;
		return $this;
	}


	/**
	 * @param Tag $tag
	 * @return $this
	 */
	public function addTag(Tag $tag) {
		$type = $tag->getTagType();
		if (!isset($this->tags[$type->getId()])) {
			$this->tags[$type->getId()] = [];
		}
		$this->tags[$type->getId()][$tag->getId()] = $tag;
		return $this;
	}


	/**
	 * @param string $text
	 * @return $this
	 */
	public function setFulltext($text) {
		$this->fulltext = trim($text);
		return $this;
	}


	/**
	 * @return \App\Model\Data\Category[]
	 */
	public function getCategories() {
		return $this->categories;
	}


	/**
	 * @return array
	 */
	public function getTags() {
		return $this->tags;
	}


	/**
	 * @return string
	 */
	public function getFulltext() {
		return $this->fulltext;
	}


	/**
	 * @return boolean
	 */
	public function isOnlyAvailable() {
		return $this->onlyAvailable;
	}


	/**
	 * @return array
	 */
	public function getSellerIds() {
		return $this->sellerIds;
	}


	/**
	 * @return array
	 */
	public function getPrice() {
		return (array)$this->price + ['from' => false, 'to' => false];
	}


	/**
	 * @param array $price
	 * @return $this
	 */
	public function setPrice(array $price) {
		$this->price = $price;
		return $this;
	}

}