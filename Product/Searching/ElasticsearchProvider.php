<?php

namespace App\Model\Data\Product\Searching;

use App\Model\Data\Tag;
use Elastica\Index;
use Elastica\QueryBuilder\DSL;
use Elastica\Type;


/**
 * ElasticsearchProvider
 *
 * @author Kryštof Česal
 */
class ElasticsearchProvider implements IProvider {

	/** @var Type */
	protected $elasticDocument;


	/**
	 * ElasticsearchProvider constructor.
	 * @param \Elastica\Index $index
	 */
	public function __construct(Index $index) {
		$this->elasticDocument = $index->getType('product');
	}


	/**
	 * @param Query $query
	 * @return array ids of products
	 */
	public function search(Query $query) {
		$must = [];
		$should = [];
		$aggs = [];
		if ($query->getFulltext()) {
			$must[] = ['name' => $query->getFulltext()];
		}
		if ($query->getCategories()) {
			$must[] = ['terms' => ['category_id' => array_keys($query->getCategories())]];
		}
		if ($query->getTags()) {
			$attributes = [];
			foreach ($query->getTags() as $typeId => $tags) {
				if (array_values($tags)[0]->getTagType()->isSelectable()) {
					$identificators = [];
					/** @var Tag $tag */
					foreach ($tags as $tag) {
						$identificators[] = 't' . $tag->getId();
					}
					$attributes[] = ['terms' => ['options.op' => $identificators]];
				}
				$must[] = ['terms' => ['tags' => array_keys($tags)]];
			}
		}
		$attributes[] = ["range" => ["options.count" => ["gt" => 0]]];
		$should[] = ["nested" => ["path" => "options", "query" => ["bool" => ["must" => $attributes]]]];
		$should[] = ['match' => ['tailored' => true]];
		$should[] = ['match' => ['after_sold_out' => true]];

		$priceLimit = [];
		if ($query->getPrice()['from']) {
			$priceLimit['gte'] = $query->getPrice()['from'];
		}
		if ($query->getPrice()['to']) {
			$priceLimit['lte'] = $query->getPrice()['to'];
		}
		if ($priceLimit) {
			$must[] = ['range' => ['price' => $priceLimit]];
		}
		if ($query->getSellerIds()) {
			$must[] = ['terms' => ['seller_id' => $query->getSellerIds()]];
		}
		$request = [
			'query' => ['bool' => ['must' => $must, 'should' => $should, 'minimum_should_match' => 1]]];
		$aggs['max_price'] = ['max' => ['field' => 'price']];
		$aggs['min_price'] = ['min' => ['field' => 'price']];
		$request += ['aggs' => $aggs];
		//\Tracy\Dumper::dump($request, [\Tracy\Dumper::DEPTH => 15]);
		$result = $this->elasticDocument->search($request);
//		\Tracy\Dumper::dump($result, [\Tracy\Dumper::DEPTH => 15]);
		$ids = [];
		foreach ($result->getResults() as $hit) {
			$ids[] = $hit->getId();
		}
		return new Result($ids, $result->getAggregation('min_price')['value'], $result->getAggregation('max_price')['value']);
	}
}