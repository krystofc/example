<?php

namespace App\Model\Data\Product\Searching;


/**
 * IProvider
 *
 * @author Kryštof Česal
 */
interface IProvider {

	/**
	 * @param Query $query
	 * @return Result
	 */
	public function search(Query $query);

}