<?php

namespace App\Model\Data\Product\Searching;

use App\Model\Data\Product\Product;


/**
 * IIndexer
 *
 * @author Kryštof Česal
 */
interface IIndexer {

	/**
	 * @param Product $product
	 * @return bool
	 */
	public function index(Product $product);

} 