<?php

namespace App\Model\Data\Product\Events;


/**
 * ProductCreatedEvent
 *
 * @author Kryštof Česal
 */
class ProductCreatedEvent extends ProductEvent {

}