<?php

namespace App\Model\Data\Product\Events;


/**
 * ProductUpdatedEvent
 *
 * @author Kryštof Česal
 */
class ProductUpdatedEvent extends ProductEvent {

}