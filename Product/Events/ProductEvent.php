<?php

namespace App\Model\Data\Product\Events;

use App\Model\Data\Product\ActiveProduct;
use Symfony\Component\EventDispatcher\Event;


/**
 * ProductEvent
 *
 * @author Kryštof Česal
 */
abstract class ProductEvent extends Event {

	/** @var ActiveProduct */
	protected $product;


	/**
	 * ProductEvent constructor.
	 * @param $product
	 */
	public function __construct(ActiveProduct $product) {
		$this->product = $product;
	}


	/**
	 * @return ActiveProduct
	 */
	public function getProduct() {
		return $this->product;
	}

}