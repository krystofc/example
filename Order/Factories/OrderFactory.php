<?php

namespace App\Model\Data;

use App\FrontModule\Model\Cart;
use App\Model\Data\Currency\CurrencyRepository;
use App\Model\Data\Order\Events\OrderCreatedEvent;
use App\Model\Data\Order\OrderLogRepository;
use App\Model\Data\Product\ProductEventDispatcher;
use App\Model\Database\Table\OrderTable;
use Nette\Database\Table\ActiveRow;
use Symfony\Component\EventDispatcher\EventDispatcher;


/**
 * OrderFactory
 *
 * @author Kryštof Česal
 */
class OrderFactory {

	/** @var OrderTable */
	protected $orderTable;

	/** @var EventDispatcher */
	protected $eventDispatcher;

	/** @var OrderPartFactory */
	protected $orderPartFactory;

	/** @var OrderLogRepository */
	protected $orderLogRepository;

	/** @var CurrencyRepository */
	protected $currencyRepository;


	/**
	 * @param ProductEventDispatcher $eventDispatcher
	 * @param OrderPartFactory $orderPartFactory
	 * @param OrderTable $orderTable
	 * @param OrderLogRepository $orderLogRepository
	 * @param CurrencyRepository $currencyRepository
	 */
	function __construct(ProductEventDispatcher $eventDispatcher, OrderPartFactory $orderPartFactory, OrderTable $orderTable, OrderLogRepository $orderLogRepository, CurrencyRepository $currencyRepository) {
		$this->eventDispatcher = $eventDispatcher;
		$this->orderPartFactory = $orderPartFactory;
		$this->orderTable = $orderTable;
		$this->orderLogRepository = $orderLogRepository;
		$this->currencyRepository = $currencyRepository;
	}


	/**
	 * @param array $orderData
	 * @param Cart $cart
	 * @return Order
	 */
	public function createNewOrder(array $orderData, Cart $cart) {
		$orderValues = array_intersect_key($orderData, array_flip(['name', 'street', 'city', 'postal_code',
																   'billing_street', 'billing_city',
																   'billing_postal_code', 'phone', 'payment']));
		$orderValues['currency_id'] = $this->currencyRepository->getActualCurrency()->getId();
		$this->orderTable->begin();
		$orderRow = $this->orderTable->insert($orderValues + ['price' => $cart->getTotalPrice()]);
		$productsSBySeller = [];
		foreach ($cart->getProducts() as $cartProduct) {
			$productsSBySeller[$cartProduct->getProduct()->getSellerId()][] = $cartProduct;
		}
		$parts = [];
		foreach ($productsSBySeller as $sellerId => $products) {
			$parts[] = $this->orderPartFactory->createNewPart($orderData['payment'], $cart->getProducts(), $sellerId, $orderRow->id);
		}
		$order = new Order($orderRow->id, $this->currencyRepository->getActualCurrency(), $this->eventDispatcher, $parts, $orderData['payment'], $cart->getTotalPrice(), $orderData, NULL, $this->orderLogRepository); // todo: support for logged users, currency
		$this->eventDispatcher->dispatch('order.created', new OrderCreatedEvent($order));
		$this->orderTable->commit();
		return $order;
	}


	/**
	 * @param ActiveRow|int $idOrRow
	 * @return Order
	 */
	public function create($idOrRow) {
		if (!$idOrRow instanceof ActiveRow) {
			$row = $this->orderTable->get($idOrRow);
		} else {
			$row = $idOrRow;
		}
		$parts = function () use ($row) {
			$parts = [];
			foreach ($row->related('order_part') as $part) {
				$parts[] = $this->orderPartFactory->create($part);
			}
			return $parts;
		};
		$userData = array_intersect_key($row->toArray(), array_flip(['name', 'street', 'city', 'postal_code',
																	 'billing_street',
																	 'billing_city', 'billing_postal_code', 'phone']));
		$currency = $this->currencyRepository->getCurrency($row->currency_id);
		$order = new Order($row->id, $currency, $this->eventDispatcher, $parts, $row->payment, $row->price, $userData, $row->user_id, $this->orderLogRepository);
		return $order;
	}

}


