<?php

namespace App\Model\Data;

use App\FrontModule\Model\CartProduct;
use App\Model\Data\Product\ProductEventDispatcher;
use App\Model\Database\Table\OrderPartTable;
use Nette\Database\IRow;
use Symfony\Component\EventDispatcher\EventDispatcher;


/**
 * OrderPartFactory
 *
 * @author Kryštof Česal
 */
class OrderPartFactory {

	/** @var OrderPartTable */
	protected $orderPartTable;

	/** @var EventDispatcher */
	protected $eventDispatcher;

	/** @var OrderProductFactory */
	protected $orderProductFactory;


	/**
	 * @param ProductEventDispatcher $eventDispatcher
	 * @param OrderPartTable $orderPartTable
	 * @param OrderProductFactory $orderProductFactory
	 */
	function __construct(ProductEventDispatcher $eventDispatcher, OrderPartTable $orderPartTable, OrderProductFactory $orderProductFactory) {
		$this->eventDispatcher = $eventDispatcher;
		$this->orderPartTable = $orderPartTable;
		$this->orderProductFactory = $orderProductFactory;
	}


	/**
	 * @param $idOrRow
	 * @return OrderPart
	 */
	public function create($idOrRow) {
		if ($idOrRow instanceof IRow) {
			$row = $idOrRow;
		} else {
			$row = $this->orderPartTable->get($idOrRow);
		}
		$products = function () use ($row) {
			$products = [];
			foreach ($row->related('order_product') as $productRow) {
				$products[] = $this->orderProductFactory->create($productRow);
			}
			return $products;
		};
		return new OrderPart($row->id, $this->eventDispatcher, $products, $this->orderPartTable, $row->seller_id, $row->status);
	}


	/**
	 * @param int $payment
	 * @param CartProduct[] $products
	 * @param $sellerId
	 * @param $orderId
	 * @return \App\Model\Data\OrderPart
	 */
	public function createNewPart($payment, $products, $sellerId, $orderId) {
		$partRow = $this->orderPartTable->insert([
			'seller_id' => $sellerId,
			'order_id' => $orderId,
			'status' => $payment == Order::PAYMENT_ON_DELIVERY ? OrderPartTable::STATUS_WAITING_TO_BE_READY : OrderPartTable::STATUS_WAITING_FOR_PAYMENT
		]);
		$orderedProducts = [];
		foreach ($products as $product) {
			$orderedProducts[] = $this->orderProductFactory->createNewProduct($product, $partRow->id);;
		}
		return new OrderPart($partRow->id, $this->eventDispatcher, $orderedProducts, $this->orderPartTable, $sellerId, $partRow->status);
	}
}