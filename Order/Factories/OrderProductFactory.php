<?php

namespace App\Model\Data;

use App\FrontModule\Model\CartProduct;
use App\Model\Data\Currency\CurrencyRepository;
use App\Model\Data\Language\LanguageRepository;
use App\Model\Database\Table\OrderProductAttributeTable;
use App\Model\Database\Table\OrderProductTable;
use Nette\Database\IRow;


/**
 * OrderProductFactory
 *
 * @author Kryštof Česal
 */
class OrderProductFactory {

	/** @var OrderProductTable */
	protected $orderProductTable;

	/** @var ProductRepository */
	protected $productRepository;

	/** @var OrderProductAttributeTable */
	protected $orderProductAttributeTable;

	/** @var CurrencyRepository */
	protected $currencyRepository;

	/** @var LanguageRepository */
	protected $languageRepository;


	function __construct(OrderProductTable $orderProductTable, ProductRepository $productRepository, OrderProductAttributeTable $orderProductAttributeTable, CurrencyRepository $currencyRepository, LanguageRepository $languageRepository) {
		$this->orderProductTable = $orderProductTable;
		$this->productRepository = $productRepository;
		$this->orderProductAttributeTable = $orderProductAttributeTable;
		$this->currencyRepository = $currencyRepository;
		$this->languageRepository = $languageRepository;
	}


	public function create($idOrRow) {
		if ($idOrRow instanceof IRow) {
			$row = $idOrRow;
		} else {
			$row = $this->orderProductTable->get($idOrRow);
		}
		$product = $this->productRepository->getProduct($row->product_id);
		$attributes = [];
		foreach ($row->related('order_product_attribute') as $attribute) {
			$attributes[$attribute->id] = $attribute->toArray();
		}
		return new OrderProduct($row->id, $product, $row->quantity, $row->stock_id, $row->total_price, $attributes);
	}


	public function createNewProduct(CartProduct $product, $partId) {
		$values = [
			'product_id' => $product->getProductId(),
			'order_part_id' => $partId,
			'quantity' => $product->quantity,
			'total_price' => $product->getTotalPrice($this->currencyRepository->getActualCurrency()),
		];
		if (!$product->getProduct()->isTailored()) {
			$stockRowId = NULL;//$this->removeFromStock($product); todo: move to subscriber
			if ($stockRowId) {
				$values += ['stock_id' => $stockRowId];
			}
		}
		$productRow = $this->orderProductTable->insert($values);
		foreach ($product->getParams() as $param) {
			$this->orderProductAttributeTable->insert([
				'order_product_id' => $productRow->id,
				'name' => $param->getAttribute()->getName($this->languageRepository->getActualLanguage()),
				'value' => $param->getName($this->languageRepository->getActualLanguage()),
				'price' => $param->getPrice($this->currencyRepository->getActualCurrency()) ?: 0,
			]);
		}
		return new OrderProduct($productRow->id, $product->getProduct(), $product->quantity, NULL, $product->getTotalPrice($this->currencyRepository->getActualCurrency()));
	}

} 