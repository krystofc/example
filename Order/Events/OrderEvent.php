<?php

namespace App\Model\Data\Order\Events;

use App\Model\Data\Order;
use Symfony\Component\EventDispatcher\Event;


/**
 * OrderEvent
 *
 * @author Kryštof Česal
 */
abstract class OrderEvent extends Event {

	/** @var Order */
	protected $order;


	function __construct(Order $order) {
		$this->order = $order;
	}


	/**
	 * @return Order
	 */
	public function getOrder() {
		return $this->order;
	}

} 