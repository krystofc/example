<?php

namespace App\Model\Data\Order\Events;

use App\Model\Data\OrderPart;


/**
 * OrderPartChangedEvent
 *
 * @author Kryštof Česal
 */
class OrderPartStatusChangedEvent extends OrderPartEvent {

	protected $oldStatus;


	function __construct(OrderPart $orderPart, $oldStatus) {
		parent::__construct($orderPart);
		$this->oldStatus = $oldStatus;
	}


	/**
	 * @return int
	 */
	public function getOldStatus() {
		return $this->oldStatus;
	}


	/**
	 * @return int
	 */
	public function getNewStatus() {
		return $this->orderPart->getStatus();
	}

}