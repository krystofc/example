<?php

namespace App\Model\Data\Order\Events;

use App\Model\Data\OrderPart;


/**
 * OrderPartEvent
 *
 * @author Kryštof Česal
 */
class OrderPartEvent extends OrderEvent {

	/** @var OrderPart */
	protected $orderPart;


	function __construct(OrderPart $orderPart) {
		parent::__construct($orderPart->getOrder());
		$this->orderPart = $orderPart;
	}


	/**
	 * @return OrderPart
	 */
	public function getOrderPart() {
		return $this->orderPart;
	}
} 