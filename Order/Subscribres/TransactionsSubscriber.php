<?php

namespace App\Model\Data\Order\Subscribers;

use App\Model\Data\Order\Events\OrderCanceledEvent;
use App\Model\Data\Order\Events\OrderCreatedEvent;
use App\Model\Data\OrderProduct;
use App\Model\Database\Table\CashTransactionTable;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;


/**
 * TransactionsListener
 *
 * @author Kryštof Česal
 */
class TransactionsSubscriber implements EventSubscriberInterface {

	/** @var CashTransactionTable */
	protected $cashTransactionTable;


	function __construct(CashTransactionTable $cashTransactionTable) {
		$this->cashTransactionTable = $cashTransactionTable;
	}


	public function onOrderCreated(OrderCreatedEvent $event) {
		foreach ($event->getOrder()->getParts() as $part) {
			/** @var OrderProduct $product */
			foreach ($part->getProducts() as $product) {
				$this->cashTransactionTable->insert([
					'amount' => $product->getSellerCommission(),
					'transaction_type' => CashTransactionTable::TYPE_ORDER,
					'user_id' => $product->getProduct()->getSellerId(),
					'blocked' => 1,
					'order_product_id' => $product->getId(),
				]);
			}
		}
	}


	public function onOrderCanceled(OrderCanceledEvent $event) {

	}


	public function onOrderCompleted() {

	}


	/**
	 * @return array The event names to listen to
	 */
	public static function getSubscribedEvents() {
		return [
			'order.created' => 'onOrderCreated',
		];
	}
}