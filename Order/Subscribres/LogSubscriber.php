<?php

namespace App\Model\Data\Order\Subscribers;

use App\Model\Data\Order\Events\OrderCreatedEvent;
use App\Model\Data\Order\Events\OrderPartStatusChangedEvent;
use App\Model\Data\Order\Log\ChangeStatusLogEvent;
use App\Model\Data\Order\Log\CreateLogEvent;
use App\Model\Data\Order\OrderLogRepository;
use App\Model\Translation\Translator;
use Nette\Utils\DateTime;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;


/**
 * LogSubscriber
 *
 * @author Kryštof Česal
 */
class LogSubscriber implements EventSubscriberInterface {

	/** @var OrderLogRepository */
	protected $orderLogRepository;

	/** @var Translator */
	protected $translator;


	function __construct(OrderLogRepository $orderLogRepository, Translator $translator) {
		$this->translator = $translator;
		$this->orderLogRepository = $orderLogRepository;
	}


	public function onStatusChanged(OrderPartStatusChangedEvent $event) {
		$event->getOrderPart();
		$params = ['oldStatus' => $event->getOldStatus(), 'newStatus' => $event->getNewStatus()];
		$this->orderLogRepository->insert(new ChangeStatusLogEvent(NULL, new DateTime, '', $params), $event->getOrderPart()
				->getId(), NULL);
	}


	public function onCreated(OrderCreatedEvent $event) {
		$this->orderLogRepository->insert(new CreateLogEvent(NULL, new DateTime, '', []), NULL, $event->getOrder()
				->getId());
	}


	/**
	 * @return array The event names to listen to
	 */
	public static function getSubscribedEvents() {
		return [
			'order.statusChanged' => 'onStatusChanged',
			'order.created' => 'onCreated',
		];
	}
}