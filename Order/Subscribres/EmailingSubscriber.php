<?php

namespace App\Model\Data\Order\Subscribers;

use App\Model\Data\Order\Events\OrderCreatedEvent;
use App\Model\Data\Order\Events\OrderPartStatusChangedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;


/**
 * EmailingListener
 *
 * @author Kryštof Česal
 */
class EmailingSubscriber implements EventSubscriberInterface {

	/**
	 * @param OrderCreatedEvent $event
	 */
	public function onOrderCreated(OrderCreatedEvent $event) {

	}


	/**
	 * @param OrderPartStatusChangedEvent $event
	 */
	public function onStatusChanged(OrderPartStatusChangedEvent $event) {

	}



	/**
	 * @return array The event names to listen to
	 */
	public static function getSubscribedEvents() {
		return [
			'order.created' => 'onOrderCreated',
			'order.statusChanged' => 'onStatusChanged',
		];
	}
}