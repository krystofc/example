<?php

namespace App\Model\Data;

use App\Model\Data\Product\ActiveProduct;


/**
 * OrderProduct
 *
 * @author Kryštof Česal
 */
class OrderProduct {

	/** @var int */
	protected $id;

	/** @var int */
	protected $totalPrice;

	/** @var array */
	protected $params;

	/** @var int */
	protected $quantity;

	/** @var int */
	protected $stockId;

	/** @var ActiveProduct */
	protected $product;


	function __construct($id, ActiveProduct $product, $quantity, $stockId = NULL, $totalPrice, $params = []) {
		$this->id = $id;
		$this->product = $product;
		$this->quantity = $quantity;
		$this->stockId = $stockId;
		$this->totalPrice = $totalPrice;
		$this->params = $params;
	}


	/**
	 * @return int
	 */
	public function getMarketCommission() {
		return $this->totalPrice * ($this->product->getCommissionRatio() / 100);
	}


	/**
	 * @return int
	 */
	public function getSellerCommission() {
		return $this->totalPrice - $this->getMarketCommission();
	}


	/**
	 * @return ActiveProduct
	 */
	public function getProduct() {
		return $this->product;
	}


	/**
	 * @return array
	 */
	public function getParams() {
		return $this->params;
	}


	/**
	 * @return int
	 */
	public function getQuantity() {
		return $this->quantity;
	}


	/**
	 * @return int
	 */
	public function getTotalPrice() {
		return $this->totalPrice;
	}


	/**
	 * @return int
	 */
	public function getId() {
		return $this->id;
	}

} 