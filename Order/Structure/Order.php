<?php

namespace App\Model\Data;

use App\Model\Data\Currency\Currency;
use App\Model\Data\Order\Log\IOrderLogEvent;
use App\Model\Data\Order\OrderLogRepository;
use Symfony\Component\EventDispatcher\EventDispatcher;


/**
 * Order
 *
 * @author Kryštof Česal
 */
class Order {

	const PAYMENT_CARD = 1;
	const PAYMENT_TRANSFER = 2;
	const PAYMENT_ON_DELIVERY = 3;

	/** @var int */
	protected $id;

	/** @var OrderPart[] */
	protected $parts = [];

	/** @var int */
	protected $price;

	/** @var int */
	protected $userId;

	/** @var Currency */
	protected $currency;

	/** @var array */
	protected $userData;

	/** @var int */
	protected $paymentMethod;

	/** @var callable|false */
	protected $partsFactory;

	/** @var EventDispatcher */
	protected $eventDispatcher;

	/** @var OrderLogRepository */
	protected $orderLogRepository;


	function __construct($id, Currency $currency, EventDispatcher $eventDispatcher, $partsFactory, $paymentMethod, $price, array $userData, $userId, OrderLogRepository $orderLogRepository) {
		$this->currency = $currency;
		$this->eventDispatcher = $eventDispatcher;
		$this->id = $id;
		if (is_array($partsFactory)) {
			$this->parts = $partsFactory;
		} else {
			$this->partsFactory = $partsFactory;
		}
		$this->paymentMethod = $paymentMethod;
		$this->price = $price;
		$this->userData = $userData;
		$this->userId = $userId;
		$this->orderLogRepository = $orderLogRepository;
	}


	/**
	 * @return int
	 */
	public function getId() {
		return $this->id;
	}


	/**
	 * @return OrderPart[]
	 */
	public function getParts() {
		if ($this->partsFactory) {
			$this->parts = call_user_func($this->partsFactory);
			foreach ($this->parts as $part) {
				$part->setOrder($this);
			}
			$this->partsFactory = false;
		}
		return $this->parts;
	}


	/**
	 * @return int
	 */
	public function getPrice() {
		return $this->price;
	}


	/**
	 * @return array
	 */
	public function getUserData() {
		return $this->userData;
	}


	/**
	 * @param $sellerId
	 * @return OrderPart|null
	 */
	public function getPartBySeller($sellerId) {
		foreach ($this->getParts() as $part) {
			if ($part->getSellerId() === $sellerId) {
				return $part;
			}
		}
		return NULL;
	}


	/**
	 * @param IOrderLogEvent $orderLog
	 */
	public function insertLog(IOrderLogEvent $orderLog) {
		$this->orderLogRepository->insert($orderLog, NULL, $this->id);
	}


	/**
	 * @return Order\Log\IOrderLogEvent[]
	 */
	public function getLogs() {
		return $this->orderLogRepository->getByOrder($this->id);
	}


	/**
	 * @return Currency
	 */
	public function getCurrency() {
		return $this->currency;
	}

} 