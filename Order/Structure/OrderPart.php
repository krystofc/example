<?php

namespace App\Model\Data;

use App\Model\Data\Order\Events\OrderPartStatusChangedEvent;
use App\Model\Database\Table\OrderPartTable;
use Nette\InvalidStateException;
use Symfony\Component\EventDispatcher\EventDispatcher;


/**
 * OrderPart
 *
 * @author Kryštof Česal
 */
class OrderPart {

	const STATUS_WAITING_FOR_PAYMENT = 1;
	const STATUS_WAITING_TO_BE_READY = 2;
	const STATUS_WAITING_FOR_COURIER = 3;
	const STATUS_WAITING_TO_BE_DELIVERED = 4;
	const STATUS_DONE = 5;

	/** @var Order */
	protected $order;

	/** @var OrderProduct[] */
	protected $products;

	/** @var callable */
	protected $productsFactory;

	/** @var int */
	protected $sellerId;

	/** @var int */
	protected $status;

	/** @var EventDispatcher */
	protected $dispatcher;

	/** @var OrderPartTable */
	protected $orderPartTable;

	/** @var int */
	protected $orderPartId;


	function __construct($partId, EventDispatcher $dispatcher, $productsFactory, OrderPartTable $orderPartTable, $sellerId, $status) {
		$this->orderPartId = $partId;
		$this->dispatcher = $dispatcher;
		if (is_array($productsFactory)) {
			$this->products = $productsFactory;
		} else {
			$this->productsFactory = $productsFactory;
		}
		$this->sellerId = $sellerId;
		$this->status = $status;
		$this->orderPartTable = $orderPartTable;
	}


	/**
	 * @return OrderProduct[]
	 */
	public function getProducts() {
		if ($this->productsFactory) {
			$this->products = call_user_func($this->productsFactory);
		}
		return $this->products;
	}


	/**
	 * @return Order
	 */
	public function getOrder() {
		return $this->order;
	}


	/**
	 * @return int
	 */
	public function getPrice() {
		$price = 0;
		foreach ($this->getProducts() as $product) {
			$price += $product->getTotalPrice();
		}
		return $price;
	}

	/**
	 * @return int
	 */
	public function getSellerId() {
		return $this->sellerId;
	}


	/**
	 * @return int
	 */
	public function getStatus() {
		return $this->status;
	}


	/**
	 * @param Order $order
	 */
	public function setOrder(Order $order) {
		$this->order = $order;
	}


	/**
	 * @param int $newStatus
	 * @throws \Exception
	 */
	public function changeStatus($newStatus) {
		try {
			if ($newStatus == $this->status) {
				throw new InvalidStateException('New status is same like old');
			}
			$oldStatus = $this->status;
			$this->orderPartTable->begin();
			$this->orderPartTable->update($this->orderPartId, ['status' => $newStatus]);
			$this->status = $newStatus;
			$this->dispatcher->dispatch('order.statusChanged', new OrderPartStatusChangedEvent($this, $oldStatus));
			$this->orderPartTable->commit();
		} catch (\Exception $e) {
			throw $e;
		}
	}


	public function getId() {
		return $this->orderPartId;
	}

} 