<?php

namespace App\Model\Data\Order;

use App\Model\Data\Order;
use App\Model\Data\Order\Log\IOrderLogEvent;
use App\Model\Data\OrderPart;
use App\Model\Database\Table\OrderLogTable;
use App\Model\Database\Table\OrderPartTable;
use App\Model\Translation\Translator;
use Nette\Database\Table\Selection;
use Nette\InvalidArgumentException;
use Nette\InvalidStateException;
use Nette\Utils\DateTime;


/**
 * OrderLogRepository
 *
 * @author Kryštof Česal
 */
class OrderLogRepository {

	/** @var OrderLogTable */
	protected $orderLogTable;

	/** @var OrderPartTable */
	protected $orderPartTable;

	/** @var Translator */
	protected $translator;


	function __construct(OrderLogTable $orderLogTable, OrderPartTable $orderPartTable, Translator $translator) {
		$this->orderLogTable = $orderLogTable;
		$this->translator = $translator;
		$this->orderPartTable = $orderPartTable;
	}


	/**
	 * @param Order $order
	 * @return IOrderLogEvent[]
	 */
	public function getByOrder(Order $order) {
		$partIds = [];
		foreach ($order->getParts() as $part) {
			$partIds = $part->getId();
		}
		return $this->createLog($this->orderLogTable->findBy('order_id = ? OR order_part_id IN ?', $order->getId(), $partIds));
	}


	/**
	 * @param OrderPart $orderPart
	 * @return IOrderLogEvent[]
	 */
	public function getByOrderPart(OrderPart $orderPart) {
		return $this->createLog($this->orderLogTable->findBy('(order_part_id = ? ) OR (order_part_id IS NULL AND order_id = ?)', $orderPart->getId(), $orderPart->getOrder()
			->getId()));
	}


	/**
	 * @param Selection $selection
	 * @return IOrderLogEvent[]
	 */
	protected function createLog(Selection $selection) {
		$logEvents = [];
		foreach ($selection as $row) {
			$className = 'App\Model\Data\Order\Log\\' . $row->type . 'LogEvent';
			if (!class_exists($className)) {
				throw new InvalidStateException("Cannot create order log event type '{$row->type}");
			}
			$params = $row->params ? unserialize($row->params) : [];
			/** @var IOrderLogEvent $log */
			$log = new $className($row->id, $row->date, $row->comment, $params);
			$log->setTranslator($this->translator);
			$logEvents[] = $log;
		}
		return $logEvents;
	}


	/**
	 * @param IOrderLogEvent $event
	 * @param int|null $partId
	 * @param int|null $orderId
	 */
	public function insert(IOrderLogEvent $event, $partId = NULL, $orderId = NULL) {
		if (!$partId && !$orderId) {
			throw new InvalidArgumentException('OrderId or PartId must be specific');
		}
		$data = $partId ? ['order_part_id' => $partId] : ['order_id' => $orderId];
		$data += [
			'params' => $event->getParams() ? serialize($event->getParams()) : NULL,
			'type' => $event->getType(),
			'date' => new DateTime,
			'comment' => $event->getComment(),
		];
		$this->orderLogTable->insert($data);
	}

} 