<?php

namespace App\Model\Data;

use App\FrontModule\Model\Cart;
use App\Model\Database\Table\OrderPartTable;
use App\Model\Database\Table\OrderTable;
use Nette\Database\Table\Selection;


/**
 * OrderRepository
 *
 * @author Kryštof Česal
 */
class OrderRepository {

	/** @var OrderTable */
	protected $orderTable;

	/** @var OrderPartTable */
	protected $orderPartTable;

	/** @var OrderFactory */
	protected $orderFactory;


	function __construct(OrderTable $orderTable, OrderPartTable $orderPartTable, OrderFactory $orderFactory) {
		$this->orderTable = $orderTable;
		$this->orderPartTable = $orderPartTable;
		$this->orderFactory = $orderFactory;
	}


	/**
	 * @param array $orderData
	 * @param Cart $cart
	 * @return Order
	 */
	public function createNewOrder($orderData, Cart $cart) {
		return $this->orderFactory->createNewOrder($orderData, $cart);
	}


	/**
	 * @param array $ids
	 * @return Order[]
	 */
	public function getByIds(array $ids) {
		$orders = [];
		foreach ($this->orderTable->findBy('id', $ids) as $row) {
			$orders[$row->id] = $this->orderFactory->create($row);
		}
		return $orders;
	}


	/**
	 * @param $id
	 * @return Order
	 */
	public function get($id) {
		return $this->orderFactory->create($id);
	}


	/**
	 * @param $orderPartId
	 * @return OrderPart|null
	 */
	public function getPart($orderPartId) {
		$partRow = $this->orderPartTable->get($orderPartId);
		if ($partRow) {
			$order = $this->orderFactory->create($partRow->order);
			$part = $order->getPartBySeller($partRow->seller_id);
			return $part;
		}
		return NULL;
	}


	/**
	 * @param Selection $selection
	 * @return OrderPart[]
	 */
	public function getPartsBySelection(Selection $selection) {
		$parts = [];
		foreach ($selection as $orderPartRow) {
			$parts[] = $this->getPart($orderPartRow);
		}
		return $parts;
	}


	/**
	 * @param $sellerId
	 * @return OrderPart[]
	 */
	public function getPartsBySeller($sellerId) {
		$selection = $this->orderPartTable->findBy('seller_id', $sellerId);
		$parts = [];
		foreach ($selection as $partRow) {
			$order = $this->orderFactory->create($partRow->order);
			$parts[$partRow->id] = $order->getPartBySeller($sellerId);
		}
		return $parts;
	}

}