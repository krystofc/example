<?php

namespace App\Model\Data\Order\Log;

use Nette\InvalidArgumentException;


/**
 * ChangeStatusLogEvent
 *
 * @author Kryštof Česal
 */
class ChangeStatusLogEvent extends BaseLogEvent {

	protected $oldStatus;

	protected $newStatus;

	protected $type = 'ChangeStatus';


	function __construct($id = NULL, $date, $comment, $params) {
		parent::__construct($id, $date, $comment, $params);
		if ($params) {
			if (isset($params['oldStatus']) && isset($params['newStatus'])) {
				$this->oldStatus = $params['oldStatus'];
				$this->newStatus = $params['newStatus'];
			}
		}
		if (!$this->newStatus) {
			throw new InvalidArgumentException("Params isn't valid");
		}
	}


	function generateMessage() {
		return 'Změna statusu z "' . $this->translator->translate('seller_order_status_' . $this->oldStatus) . '" na "' . $this->translator->translate('seller_order_status_' . $this->newStatus) . '"';
	}


	function getParams() {
		return ['oldStatus' => $this->oldStatus, 'newStatus' => $this->newStatus];
	}

}