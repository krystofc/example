<?php

namespace App\Model\Data\Order\Log;

use Nette\Localization\ITranslator;


/**
 * IOrderLogEvent
 *
 * @author Kryštof Česal
 */
interface IOrderLogEvent {

	function __construct($id = NULL, $date, $comment, $params);


	function generateMessage();


	function getId();


	function getDate();


	function getComment();


	function getParams();


	function getType();


	function setTranslator(ITranslator $translator);

} 