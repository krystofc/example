<?php

namespace App\Model\Data\Order\Log;

use Nette\InvalidStateException;
use Nette\Localization\ITranslator;


/**
 * BaseLogEvent
 *
 * @author Kryštof Česal
 */
abstract class BaseLogEvent implements IOrderLogEvent {

	protected $id;

	protected $date;

	protected $comment;

	protected $type;

	/** @var ITranslator */
	protected $translator;


	function __construct($id = NULL, $date, $comment, $params) {
		$this->comment = $comment;
		$this->date = $date;
		$this->id = $id;
		if (!$this->type) {
			throw new InvalidStateException('A type name must be specific in' . get_class($this));
		}
	}


	/**
	 * @return mixed
	 */
	public function getComment() {
		return $this->comment;
	}


	/**
	 * @return mixed
	 */
	public function getDate() {
		return $this->date;
	}


	/**
	 * @return mixed
	 */
	public function getId() {
		return $this->id;
	}


	/**
	 * @return mixed
	 */
	public function getType() {
		return $this->type;
	}


	/**
	 * @param ITranslator $translator
	 */
	public function setTranslator(ITranslator $translator) {
		$this->translator = $translator;
	}

} 