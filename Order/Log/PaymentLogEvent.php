<?php

namespace App\Model\Data\Order\Log;

use Nette\InvalidArgumentException;


/**
 * PaymentLogEvent
 *
 * @author Kryštof Česal
 */
class PaymentLogEvent extends BaseLogEvent {

	const METHOD_BANK_TRANSFER = 1;
	const METHOD_CARD = 2;

	protected $type = 'Payment';

	protected $paymentMethod;


	function __construct($id = NULL, $date, $comment, $params) {
		parent::__construct($id, $date, $comment, $params);
		if ($params && isset($params['paymentMethod'])) {
			$this->paymentMethod = $params['paymentMethod'];
		} else {
			throw new InvalidArgumentException("Params isn't valid");
		}
	}


	public function generateMessage() {
		return 'Přijata platba přes ' . $this->translator->translate('log_event_payment_method_' . $this->paymentMethod);
	}


	public function getParams() {
		return ['paymentMethod' => $this->paymentMethod];
	}
}