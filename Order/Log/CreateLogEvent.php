<?php

namespace App\Model\Data\Order\Log;


/**
 * CreateStatusLogEvent
 *
 * @author Kryštof Česal
 */
class CreateLogEvent extends BaseLogEvent {

	protected $type = 'Create';


	function generateMessage() {
		return 'Vytvoření nové objednávky';
	}


	function getParams() {
		return [];
	}
}